<!DOCTYPE html>
<html lang="en">

<!-- Top Head -->
<?php include("incs/header-top.html") ?>
<!-- /Top Head -->

<body id="app-container" class="menu-sub-hidden show-spinner">
    <?php include("incs/header.html") ?>
    <?php include("incs/sidebar-left.html") ?>

    <main>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="mb-2">
                        <h1 class="text-primary">Set Document Header</h1>
						
						<?php /*?><nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
							<ol class="breadcrumb pt-0">
								<!--<li class="breadcrumb-item">
									<a href="#">Home</a>
								</li>-->
								<li class="breadcrumb-item">
									<a href="#">Import E-Stamp</a>
								</li>
								<li class="breadcrumb-item active text-gray" aria-current="page">Upload File</li>
								
							</ol>
						</nav><?php */?>

                    </div>
					
					
					
					<div class="card mb-3">
					<div class="card-body p-3">
							<div class="search-bar">
							<a class="btn p-2 d-inline-block d-md-none" data-toggle="collapse" href="#searchOptions" role="button" aria-expanded="true" aria-controls="searchOptions">
								Display Options
								<i class="simple-icon-arrow-down align-middle"></i>
							</a>
							<div id="searchOptions">
								<div class="d-flex flex-wrap row align-items-end">
									<div class="col-12 col-sm input-group flex-nowrap mb-2">
										<?php /*?><div class="input-group-append">
											<span class="input-group-text border-0 pb-0 d-flex align-item-end"><i class="simple-icon-magnifier"></i></span>
										</div>
										<label class="form-group has-float-label">
											<input class="form-control border-top-0 border-left-0 border-right-0" placeholder="Search...">
										</label><?php */?>
										
										
										<select class="custom-select border-top-0 border-left-0 border-right-0" required="">
											<option value="" selected="">iRich Co.ltd., สำนักงานใหญ่ (00000)</option>
											<option value="1">One</option>
											<option value="2">Two</option>
											<option value="3">Three</option>
										</select>
									</div>
									
									
									<div class="top-right-button-container text-nowrap col-12 col-sm-auto mb-2">

												<a class="btn btn-success btn-md top-right-button rounded-05" href="javascript:;" data-toggle="modal" data-target="#addHeadTMPModal" style="min-width: 120px"><i class="icon-img f-white mt-1n"><img src="di/ic-plus.png" height="16"></i> Add</a>
									</div>
								</div>
							</div>
							</div>
										

							
							<div class="dc-add-tb dc-min-h ds-table mt-4">
								<table class="table table-striped table-responsive-xs">
										<thead>
											<tr>
												<th scope="col">Index</th>
												<th scope="col">Bean</th>
												<th scope="col">Column name</th>
												<th scope="col">Type</th>
												<th scope="col">Filter</th>
												<th scope="col">Package</th>
												<th scope="col">Status</th>
												<th scope="col" class="text-center">Action</th>
											</tr>
										</thead>
										<tbody>
											<?php for($i=1;$i<=10;$i++){ ?>
											<tr class="tb-list">
												<td valign="middle" class="text-left"><?php echo $i; ?></td>
												<td valign="middle" class="text-left">document_id</td>
												<td valign="middle" class="text-left">เลขที่เอกสาร</td>
												<td valign="middle" class="text-left">varchar</td>
												<td valign="middle" class="text-left">
													<?php if($i%2==0){ ?>-
													<?php } else { ?>code
													<?php }?>
												</td>
												<td valign="middle" class="text-left">
													<?php if($i%2==0){ ?>-
													<?php } else { ?>DocumentTypeEnitry
													<?php }?>
												</td>
												<td valign="middle" class="text-left">Active</td>
												<td class="text-center" valign="middle">
														<a href="javascript:;" data-toggle="modal" data-target="#editHeadTMPModal"  style="min-width: 90px" class="btn btn-warning btn-md p-1 rounded-05 mr-2" title="Edit">Edit</a>
														<a href="javascript:;" style="min-width: 90px" class="btn btn-outline-primary btn-md p-1 rounded-05" title="Close">Close</a>
												</td>
												

												
												
											</tr>
											<?php } ?>
											
											
											
										</tbody>
									</table>
							</div>
							

							
							<div class="ft-paging d-flex justify-content-between align-items-center">
								<div class="dropdown-as-select display-page" id="pageCount">
									<span class="text-black text-small">1-10 of 195 items</span>
								</div>
								<div class="d-block d-md-inline-block ml-5">
									<nav class="ctrl-page d-flex flex-nowrap align-items-center">
										<ul class="pagination justify-content-center mb-0">
										   <!-- <li class="page-item ">
												<a class="page-link first" href="#">
													<i class="simple-icon-control-start"></i>
												</a>
											</li>-->
											<li class="page-item ">
												<a class="page-link prev" href="#">
													<i class="simple-icon-arrow-left"></i>
												</a>
											</li>
											<li class="page-item active">
												<a class="page-link" href="#">1</a>
											</li>
											<li class="page-item ">
												<a class="page-link" href="#">2</a>
											</li>
											<li class="page-item">
												<a class="page-link" href="#">3</a>
											</li>
											<li class="page-item">
												<a class="page-link" href="#">4</a>
											</li>
											<li class="page-item">
												<a class="page-link" href="#">5</a>
											</li>
											<li class="page-item">
												<span class="page-link">...</span>
											</li>
											<li class="page-item">
												<a class="page-link" href="#">19</a>
											</li>
											<li class="page-item ">
												<a class="page-link next" href="#" aria-label="Next">
													<i class="simple-icon-arrow-right"></i>
												</a>
											</li>
											<!--<li class="page-item ">
												<a class="page-link last" href="#">
													<i class="simple-icon-control-end"></i>
												</a>
											</li>-->
										</ul>
										
											<button class="btn btn-outline-dark btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
												12
											</button>
											<div class="dropdown-menu dropdown-menu-right">
												<a class="dropdown-item" href="#">5</a>
												<a class="dropdown-item active" href="#">12</a>
												<a class="dropdown-item" href="#">24</a>
											</div>
									</nav>
								</div>
							</div>

							
					</div>
					</div>
					
					<!-- end -->

                </div>
            </div>
        </div>

    </main>

    <!-- Modal -->
<div class="modal fade" id="addHeadTMPModal" tabindex="-1" role="dialog"	aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">

			<div class="modal-body pt-5">
			   <div class="row">
			   		
					<div class="col-6 mb-3">
						<label class="form-group w-100 mb-0">
								<input type="text" class="input-sm form-control rounded-05" placeholder="Column Index *">
						</label>
					</div>
					<div class="col-6 mb-3">
						<label class="form-group w-100 mb-0">
								<input type="text" class="input-sm form-control rounded-05" placeholder="Message name *">
						</label>
					</div>
					
					<div class="col-6 mb-3">
						<label class="form-group w-100 mb-0">
							<select class="form-control select2-normal" data-width="100%" data-placeholder="Type *">
								<option></option>
								<option>option 1</option>
								<option>option 2</option>
							</select>
						</label>		
					</div>
					<div class="col-6 mb-3">
						<label class="form-group w-100 mb-0">
							<select class="form-control select2-normal" data-width="100%" data-placeholder="Bean *">
								<option></option>
								<option>option 1</option>
								<option>option 2</option>
							</select>
						</label>		
					</div>
					
					<div class="col-6 mb-3">
						<label class="form-group w-100 mb-0">
							<select class="form-control select2-normal" data-width="100%" data-placeholder="Package">
								<option></option>
								<option>option 1</option>
								<option>option 2</option>
							</select>
						</label>		
					</div>
					<div class="col-6 mb-3">
						<label class="form-group w-100 mb-0">
							<select class="form-control select2-normal" data-width="100%" data-placeholder="Name">
								<option></option>
								<option>option 1</option>
								<option>option 2</option>
							</select>
						</label>		
					</div>

			   </div>
			   <div class="custom-control custom-checkbox d-inline-block">
					<input type="checkbox" class="custom-control-input" id="jQueryCustomCheck1" name="jQueryCheckbox">
					<label class="custom-control-label" for="jQueryCustomCheck1">Require document id ( Check Duplicate Document ID)</label>
				</div>
			</div>
			<div class="modal-footer pt-2 d-flex justify-content-end border-0">
				<button type="button" class="btn btn-md btn-outline-primary rounded-05 col-3" data-dismiss="modal">Cancel</button>
				<button type="button" class="btn btn-md btn-gray rounded-05 col-3 disabled" data-dismiss="modal" data-toggle="modal" data-target="#saveModalsuccess">Confirm</button>			
				<!--<button type="button" class="btn btn-md btn-primary rounded-05 col-3" data-dismiss="modal" data-toggle="modal" data-target="#SaveCPNModalsuccess">Confirm</button>		-->	
			</div>
		</div>
	</div>
</div>


<div class="modal fade" id="editHeadTMPModal" tabindex="-1" role="dialog"	aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">

			<div class="modal-body pt-5">
			   <div class="row">
			   		<div class="col-6 mb-3">
						<label class="form-group mb-0 has-float-label">
								<input type="text" class="input-sm form-control rounded-05" value="0">
								<span>Template Name *</span>
						</label>
					</div>
					<div class="col-6 mb-3">
						<label class="form-group mb-0 has-float-label">
								<input type="text" class="input-sm form-control rounded-05" value="เลขที่เอกสาร">
								<span>Template Name *</span>
						</label>
					</div>
					
					<div class="col-6 mb-3">
						<label class="form-group has-float-label mb-0">
							<select class="form-control select2-normal" data-width="100%" data-placeholder="Type *">
								<option></option>
								<option selected>varchar</option>
								<option>option 1</option>
								<option>option 2</option>
							</select>
							<span>Company *</span>
						</label>
					</div>
					<div class="col-6 mb-3">
						<label class="form-group has-float-label mb-0">
							<select class="form-control select2-normal" data-width="100%" data-placeholder="Bean *">
								<option></option>
								<option selected>document_id</option>
								<option>option 1</option>
								<option>option 2</option>
							</select>
							<span>Company *</span>
						</label>
					</div>
					
					<div class="col-6 mb-3">
						<label class="form-group has-float-label mb-0">
							<select class="form-control select2-normal" data-width="100%" data-placeholder="Package">
								<option></option>
								<option selected>iRich Co.ltd.,สำนักงานใหญ่(00000)</option>
								<option>option 1</option>
								<option>option 2</option>
							</select>
							<span>Company *</span>
						</label>
					</div>
					<div class="col-6 mb-3">
						<label class="form-group has-float-label mb-0">
							<select class="form-control select2-normal" data-width="100%" data-placeholder="Name">
								<option></option>
								<option selected>Name</option>
								<option>option 1</option>
								<option>option 2</option>
							</select>
							<span>Company *</span>
						</label>
					</div>
					

			   </div>
			   <div class="custom-control custom-checkbox d-inline-block">
					<input type="checkbox" class="custom-control-input" id="jQueryCustomCheck1" name="jQueryCheckbox" checked="">
					<label class="custom-control-label" for="jQueryCustomCheck1">Require document id ( Check Duplicate Document ID)</label>
				</div>
			</div>
			<div class="modal-footer pt-2 d-flex justify-content-end border-0">
				<button type="button" class="btn btn-md btn-outline-primary rounded-05 col-3" data-dismiss="modal">Cancel</button>
				<button type="button" class="btn btn-md btn-primary rounded-05 col-3" data-dismiss="modal" data-toggle="modal" data-target="#saveModalsuccess">Confirm</button>				
			</div>
		</div>
	</div>
</div>
<!-- /Modal -->    

    <?php include("incs/js.html") ?>
	<?php include("incs/modal.html") ?>
	
	
    <script src="js/vendor/bootstrap-datepicker.js"></script>
    <script src="js/dore.script.js"></script>
    <script src="js/scripts.js"></script>
	<script>
	$(document).ready(function() {
	   $('.select2-normal').select2({
			//placeholder: 'Content Language',
			minimumResultsForSearch: -1,
			//width: 350
		});
	} );
	</script>
<script>
$(document).ready(function() {
	$('.main-menu>.scroll>.list-unstyled>li>a.rotate-arrow-icon').addClass('collapsed');
	$('.main-menu>.scroll>.list-unstyled>li>.collapse').removeClass('show');
	$('.main-menu .inner-level-menu>li').removeClass('active');
	$('.main-menu>.scroll>.list-unstyled>li:nth-child(7)>a.rotate-arrow-icon').removeClass('collapsed');
	$('.main-menu>.scroll>.list-unstyled>li:nth-child(7)>.collapse').addClass('show');
	$('.main-menu>.scroll>.list-unstyled>li:nth-child(7) .inner-level-menu>li:nth-child(4)').addClass('active');
});
</script>
</body>

</html>