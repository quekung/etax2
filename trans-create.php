<!DOCTYPE html>
<html lang="en">

<!-- Top Head -->
<?php include("incs/header-top.html") ?>
<!-- /Top Head -->

<body id="app-container" class="menu-sub-hidden show-spinner">
    <?php include("incs/header.html") ?>
    <?php include("incs/sidebar-left.html") ?>

    <main>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="mb-2">
                        <h1 class="text-primary">Creat EBMS</h1>
						
						<?php /*?><nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
							<ol class="breadcrumb pt-0">
								<!--<li class="breadcrumb-item">
									<a href="#">Home</a>
								</li>-->
								<li class="breadcrumb-item">
									<a href="#">Import E-Stamp</a>
								</li>
								<li class="breadcrumb-item active text-gray" aria-current="page">Upload File</li>
								
							</ol>
						</nav><?php */?>

                    </div>
					
					
					
					<div class="card mb-3">
					<div class="card-body p-3">
						<div id="smart" class="sw-main sw-theme-dots">
                            <ul class="card-header nav nav-tabs step-anchor step-tabs" role="tablist">
                                <li class="nav-item active">
									<a id="first-tab" data-toggle="tab" role="tab" aria-controls="dotStep1" aria-selected="true" href="#dotStep1" class="nav-link">1</a>
								</li>
                                <li class="nav-item">
									<a id="second-tab" href="trans-create2.php" class="nav-link">2</a>
								</li>
                            </ul>
							


                            <div class="card-body">
                                <div id="dotStep1" class="tab-pane show active" role="tabpanel" aria-labelledby="first-tab">
                                    <h3 class="pb-2 h6">Search Document</h3>
                                    <div class="row mb-3">
										<div class="col-6">
											<div class="input-group date">
													<input type="text" class="input-sm form-control border-right-0" name="start" placeholder="Start date *" />
													<span class="input-group-text input-group-append input-group-addon border-left-0">
														<i class="simple-icon-calendar"></i>
													</span>
												</div>

										</div>
										<div class="col-6">
											<div class="input-group date">
													<input type="text" class="input-sm form-control border-right-0" name="end" placeholder="End date *" />
													<span class="input-group-text input-group-append input-group-addon border-left-0">
														<i class="simple-icon-calendar"></i>
													</span>
												</div>
										</div>
									</div>
									
									<div class="row mb-3">
										<div class="col-6">
												<label class="hid">Document</label>
												<select class="form-control select2-normal" data-width="100%" data-placeholder="Document">
													<option></option>
													<option>option 1</option>
													<option>option 2</option>
												</select>
										</div>
										<div class="col-6">
											<div class="input-group addon">
													<input type="text" class="input-sm form-control border-right-0" name="end" placeholder="Document Number" />
													<span class="input-group-text input-group-append input-group-addon border-left-0">
														<i class="simple-icon-magnifier"></i>
													</span>
												</div>
										</div>
									</div>
									
									<div class="row mb-3">
										<div class="col-6">
											<label class="form-group has-float-label">
												<div class="input-group addon">
													<input type="text" class="input-sm form-control border-right-0" name="end" placeholder="Search Tag ID" value="9999999999" />
													<span class="input-group-text input-group-append input-group-addon border-left-0">
														<i class="simple-icon-magnifier"></i>
													</span>
												</div>
												<span>Tag ID *</span>
											</label>
										</div>
									</div>
									
									<div id="compare">
										
										<div class="row">
											<div class="col">
												<h3 class="pb-2 h6">Search Document</h3>
												<div class="border-light border rounded-05 p-2 h-100">
													<div class="custom-control custom-checkbox mb-1">
														<input type="checkbox" class="custom-control-input" id="branch1">
														<label class="custom-control-label" for="branch1">สาขา 1</label>
													</div>
													<div class="custom-control custom-checkbox mb-1">
														<input type="checkbox" class="custom-control-input" id="branch2">
														<label class="custom-control-label" for="branch2">สาขา 2</label>
													</div>
													<div class="custom-control custom-checkbox mb-1">
														<input type="checkbox" class="custom-control-input" id="branch3">
														<label class="custom-control-label" for="branch3">สาขา 3</label>
													</div>
												</div>
											</div>
											<div class="col-auto pt-5">
												<div class="mb-1"><button class="btn btn-xs btn-gray2 rounded-05 pt-0 pb-0" type="button"><i class="iconsminds-arrow-right"></i></button></div>
												<div class="mb-1"><button class="btn btn-xs btn-blue rounded-05 pt-0 pb-0" type="button"><i class="iconsminds-arrow-right-2"></i></button></div>
												<div class="mb-1"><button class="btn btn-xs btn-gray2 rounded-05 pt-0 pb-0" type="button"><i class="iconsminds-arrow-left"></i></button></div>
											</div>
											<div class="col">
												<h3 class="pb-2 h6">Choosed Branch</h3>
												<div class="border-light border rounded-05 p-2 h-100">
													
												</div>
											</div>
											
										</div>
									</div>
									
									<div class="ctrl-btn d-flex justify-content-end clearfix mt-5">
										<a href="trans-create2.php" class="btn btn-primary btn-lg rounded-05">Search</a>
									</div>
									
                                </div>
								
                            </div>
                        </div>

							
					</div>
					
	
					
					</div>
					
					<!-- end -->

                </div>
            </div>
        </div>

    </main>

    

    <?php include("incs/js.html") ?>
	<?php include("incs/modal.html") ?>
<!-- Modal -->
<div class="modal fade" id="viewResultModal" tabindex="-1" role="dialog"	aria-hidden="true">
	<div class="modal-dialog" role="document">
		
		<div class="modal-content">
			<div class="modal-header  pt-3 pb-2 border-0">
				<h5 class="modal-title p-2">Search Result</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body pt-0 pb-5">
			   <div class="dc-add-tb  ds-table mt-0">
					<table class="table table-striped table-responsive-xs">
							<thead>
								<tr>

									<th scope="col">
										<label class="custom-control custom-checkbox mb-0 mr-n3 d-inline-block">
											<input type="checkbox" class="custom-control-input">
											<span class="custom-control-label">&nbsp;</span>
										</label>
									</th>
									<th scope="col">Document Type</th>
									<th scope="col">Files Amount</th>
								</tr>
							</thead>
							<tbody>
								<?php for($i=1;$i<=1;$i++){ ?>
								<tr>
									<td>
										<label class="custom-control custom-checkbox mb-0 mr-n3 d-inline-block">
											<input type="checkbox" class="custom-control-input">
											<span class="custom-control-label">&nbsp;</span>
										</label>
									</td>
									<td>
										Tax Invoice
									</td>
									<td>
										8
									</td>
								</tr>
								<?php } ?>
							</tbody>
					</table>
				</div>
			   
			   
			</div>
			<div class="modal-footer pt-2 d-flex justify-content-end border-0">
				<button type="button" class="btn btn-md btn-outline-primary rounded-05 col-3">View</button>	 <button type="button" class="btn btn-md btn-primary rounded-05 col-3">Confirm</button>			
			</div>

		</div>
	</div>
</div>
<!-- /Modal -->

	
	<script src="js/vendor/jquery.smartWizard.min.js"></script>
    <script src="js/vendor/bootstrap-datepicker.js"></script>
    <script src="js/dore.script.js"></script>
    <script src="js/scripts.js"></script>
	<script>
	$(document).ready(function() {
	   $('.select2-normal').select2({
			//placeholder: 'Content Language',
			minimumResultsForSearch: -1,
			//width: 350
		});
	} );
	</script>
<script>
$(document).ready(function() {
	$('.main-menu>.scroll>.list-unstyled>li>a.rotate-arrow-icon').addClass('collapsed');
	$('.main-menu>.scroll>.list-unstyled>li>.collapse').removeClass('show');
	$('.main-menu .inner-level-menu>li').removeClass('active');
	$('.main-menu>.scroll>.list-unstyled>li:nth-child(4)>a.rotate-arrow-icon').removeClass('collapsed');
	$('.main-menu>.scroll>.list-unstyled>li:nth-child(4)>.collapse').addClass('show');
	$('.main-menu>.scroll>.list-unstyled>li:nth-child(4) .inner-level-menu>li:nth-child(1)').addClass('active');
});
</script>
</body>

</html>