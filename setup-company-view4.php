<!DOCTYPE html>
<html lang="en">

<!-- Top Head -->
<?php include("incs/header-top.html") ?>
<!-- /Top Head -->

<body id="app-container" class="menu-sub-hidden show-spinner">
    <?php include("incs/header.html") ?>
    <?php include("incs/sidebar-left.html") ?>

    <main>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="mb-2">
                        <h1 class="text-primary">iRich Co.ltd.,</h1>
						
						<?php /*?><nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
							<ol class="breadcrumb pt-0">
								<!--<li class="breadcrumb-item">
									<a href="#">Home</a>
								</li>-->
								<li class="breadcrumb-item">
									<a href="#">Import E-Stamp</a>
								</li>
								<li class="breadcrumb-item active text-gray" aria-current="page">Upload File</li>
								
							</ol>
						</nav><?php */?>
						
						<div class="top-right-button-container">
							<a href="setup-company-create4.php" style="min-width: 90px;" class="btn btn-warning btn-sm rounded-05" readonly>Edit</a>
						</div>

                    </div>
					
					
					
					<div class="card mb-3">
					<div class="card-body p-3">
						<div id="smart" class="sw-main sw-theme-dots">
                            <ul class="card-header nav nav-tabs step-anchor step-tabs" role="tablist">
                                <li class="nav-item">
									<a id="first-tab" href="setup-company-view.php" class="nav-link text">
									<i>1</i>
									<span>General Profile</span>
									</a>
								</li>
                                <li class="nav-item">
									<a id="second-tab" href="setup-company-view2.php" class="nav-link text">
									<i>2</i>
									<span>Branch Profile</span>
									</a>
								</li>
								 <li class="nav-item">
									<a id="second-tab" href="setup-company-view3.php" class="nav-link text">
									<i>3</i>
									<span>Email Server</span>
									</a>
								</li>
								 <li class="nav-item active">
									<a id="second-tab" href="setup-company-view4.php" class="nav-link text">
									<i>4</i>
									<span>Config CA</span>
									</a>
								</li>
                            </ul>
							


                            <div class="card-body">
                                <div id="dotStep1" class="tab-pane show active" role="tabpanel" aria-labelledby="first-tab">
      
									
									<div class="row">
										<div class="col-6 mb-3">
											<label class="form-group mb-0 has-float-label">
													<input type="text" class="input-sm form-control rounded-05 bg-white text-gray" readonly value="105560175152">
												<span>Company ID *</span>
											</label>
										</div>
										<div class="col-6 mb-3">
											<label class="form-group mb-0 has-float-label">
													<input type="text" class="input-sm form-control rounded-05 bg-white text-gray" readonly value="/usr/lib/libCryptoki2_64.so">
												<span>CA Path *</span>
											</label>
										</div>
										
										<div class="col-6 mb-3">
											<label class="form-group mb-0 has-float-label">
													<input type="text" class="input-sm form-control rounded-05 bg-white text-gray" readonly value="1">
												<span>Slot ID *</span>
											</label>
										</div>
										<div class="col-6 mb-3">
											<label class="form-group mb-0 has-float-label">
													<input type="text" class="input-sm form-control rounded-05 bg-white text-gray" readonly value="w]V?V3D`9!(gj#%p">
												<span>PIN *</span>
											</label>
										</div>
										
										
									</div>
									
									

									
									<div class="ctrl-btn d-flex justify-content-end mt-1">
										
										
										<div class="top-right-button-container">
										<a href="setup-company-view3.php" class="btn btn-outline-primary btn-lg rounded-05 mr-2">Previous</a>
										<a href="setup-company-view.php" class="btn btn-primary btn-lg rounded-05">Next</a>
										</div>
									</div>
									
                                </div>
								
                            </div>
                        </div>

							
					</div>
					
	
					
					</div>
					
					<!-- end -->

                </div>
            </div>
        </div>

    </main>

    

    <?php include("incs/js.html") ?>
	<?php include("incs/modal.html") ?>


	
	<script src="js/vendor/jquery.smartWizard.min.js"></script>
    <script src="js/vendor/bootstrap-datepicker.js"></script>
    <script src="js/dore.script.js"></script>
    <script src="js/scripts.js"></script>
	<script>
	$(document).ready(function() {
	   $('.select2-normal').select2({
			//placeholder: 'Content Language',
			minimumResultsForSearch: -1,
			//width: 350
		});
	} );
	</script>
<script>
$(document).ready(function() {
	$('.main-menu>.scroll>.list-unstyled>li>a.rotate-arrow-icon').addClass('collapsed');
	$('.main-menu>.scroll>.list-unstyled>li>.collapse').removeClass('show');
	$('.main-menu .inner-level-menu>li').removeClass('active');
	$('.main-menu>.scroll>.list-unstyled>li:nth-child(8)>a.rotate-arrow-icon').removeClass('collapsed');
	$('.main-menu>.scroll>.list-unstyled>li:nth-child(8)>.collapse').addClass('show');
	$('.main-menu>.scroll>.list-unstyled>li:nth-child(8) .inner-level-menu>li:nth-child(1)').addClass('active');
});
</script>
</body>

</html>