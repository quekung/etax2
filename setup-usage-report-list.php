<!DOCTYPE html>
<html lang="en">

<!-- Top Head -->
<?php include("incs/header-top.html") ?>
<!-- /Top Head -->

<body id="app-container" class="menu-sub-hidden show-spinner">
    <?php include("incs/header.html") ?>
    <?php include("incs/sidebar-left.html") ?>

    <main>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="mb-2">
                        <h1 class="text-primary">Usage Report List</h1>
						
						<?php /*?><nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
							<ol class="breadcrumb pt-0">
								<!--<li class="breadcrumb-item">
									<a href="#">Home</a>
								</li>-->
								<li class="breadcrumb-item">
									<a href="#">Import E-Stamp</a>
								</li>
								<li class="breadcrumb-item active text-gray" aria-current="page">Upload File</li>
								
							</ol>
						</nav><?php */?>

                    </div>
					
					
					
					<div class="card mb-3">
					<div class="card-body p-3">

										
							<div class="headbar-tb mb-3 mb-0 d-flex justify-content-end align-items-center">

								<div class="top-right-button-container">
									<a href="javascript:;" class="btn btn-danger-50 top-right-button rounded-05 mr-1"><i class="icon-img mt-1n"><img src="di/ic-pdf-red.png" height="16"></i> PDF</a>
									<a href="javascript:;" class="btn btn-success-50 top-right-button rounded-05 mr-1"><i class="icon-img mt-1n"><img src="di/ic-xml-green.png" height="16"></i> XLSX</a>
									

								</div>
							</div>		
							

							
							<div class="dc-add-tb dc-min-h ds-table">
								<table class="table table-striped table-responsive-xs">
										<thead>
											<tr>

												<th scope="col">Company</th>
												<th scope="col">Branch</th>
												<th scope="col">Date</th>
												<th scope="col">Create XML</th>
												<th scope="col">Create PDF</th>
												<th scope="col">Send E-Mail PDF</th>
												<th scope="col">Send RD XML</th>
											</tr>
										</thead>
										<tbody>
											<!--<tr id="default" class="no-data">
												<td valign="middle" colspan="6" class="text-center">
													<div class="p-5"><img src="di/ic-not-found.png" height="150" alt="no result"></div>
												</td>
											</tr>-->
	
											<?php for($i=1;$i<=10;$i++){ ?>
											<tr class="tb-list">
												<td valign="middle" class="text-left">บริษัท แม็คโครอาร์โอเอช จำกัด</td>
												<td valign="middle" class="text-left">00002</td>
												<td valign="middle" class="text-left">25/08/2020</td>
												<td valign="middle" class="text-left">0</td>
												<td valign="middle" class="text-left">0</td>
												<td valign="middle" class="text-left">0</td>
												<td valign="middle" class="text-left">0</td>
											</tr>
											<?php } ?>
											
											
											
										</tbody>
									</table>
							</div>
							

							
							<div class="ft-paging d-flex justify-content-between align-items-center">
								<div class="dropdown-as-select display-page" id="pageCount">
									<span class="text-black text-small">1-10 of 195 items</span>
								</div>
								<div class="d-block d-md-inline-block ml-5">
									<nav class="ctrl-page d-flex flex-nowrap align-items-center">
										<ul class="pagination justify-content-center mb-0">
										   <!-- <li class="page-item ">
												<a class="page-link first" href="#">
													<i class="simple-icon-control-start"></i>
												</a>
											</li>-->
											<li class="page-item ">
												<a class="page-link prev" href="#">
													<i class="simple-icon-arrow-left"></i>
												</a>
											</li>
											<li class="page-item active">
												<a class="page-link" href="#">1</a>
											</li>
											<li class="page-item ">
												<a class="page-link" href="#">2</a>
											</li>
											<li class="page-item">
												<a class="page-link" href="#">3</a>
											</li>
											<li class="page-item">
												<a class="page-link" href="#">4</a>
											</li>
											<li class="page-item">
												<a class="page-link" href="#">5</a>
											</li>
											<li class="page-item">
												<span class="page-link">...</span>
											</li>
											<li class="page-item">
												<a class="page-link" href="#">19</a>
											</li>
											<li class="page-item ">
												<a class="page-link next" href="#" aria-label="Next">
													<i class="simple-icon-arrow-right"></i>
												</a>
											</li>
											<!--<li class="page-item ">
												<a class="page-link last" href="#">
													<i class="simple-icon-control-end"></i>
												</a>
											</li>-->
										</ul>
										
											<button class="btn btn-outline-dark btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
												12
											</button>
											<div class="dropdown-menu dropdown-menu-right">
												<a class="dropdown-item" href="#">5</a>
												<a class="dropdown-item active" href="#">12</a>
												<a class="dropdown-item" href="#">24</a>
											</div>
									</nav>
								</div>
								
								
							</div>

							
					</div>
					</div>
					
					<!-- end -->

                </div>
            </div>
        </div>

    </main>

    

    <?php include("incs/js.html") ?>
	<?php include("incs/modal.html") ?>
	
    <script src="js/vendor/bootstrap-datepicker.js"></script>
    <script src="js/dore.script.js"></script>
    <script src="js/scripts.js"></script>
	<script>
	$(document).ready(function() {
	   $('.select2-normal').select2({
			//placeholder: 'Content Language',
			minimumResultsForSearch: -1,
			//width: 350
		});
	} );
	</script>
<script>
$(document).ready(function() {
	$('.main-menu>.scroll>.list-unstyled>li>a.rotate-arrow-icon').addClass('collapsed');
	$('.main-menu>.scroll>.list-unstyled>li>.collapse').removeClass('show');
	$('.main-menu .inner-level-menu>li').removeClass('active');
	$('.main-menu>.scroll>.list-unstyled>li:nth-child(8)>a.rotate-arrow-icon').removeClass('collapsed');
	$('.main-menu>.scroll>.list-unstyled>li:nth-child(8)>.collapse').addClass('show');
	$('.main-menu>.scroll>.list-unstyled>li:nth-child(8) .inner-level-menu>li:nth-child(3)').addClass('active');
});
</script>
</body>

</html>