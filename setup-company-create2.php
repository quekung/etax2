<!DOCTYPE html>
<html lang="en">

<!-- Top Head -->
<?php include("incs/header-top.html") ?>
<!-- /Top Head -->

<body id="app-container" class="menu-sub-hidden show-spinner">
    <?php include("incs/header.html") ?>
    <?php include("incs/sidebar-left.html") ?>

    <main>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="mb-2">
                        <h1 class="text-primary">iRich Co.ltd.,</h1>
						
						<?php /*?><nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
							<ol class="breadcrumb pt-0">
								<!--<li class="breadcrumb-item">
									<a href="#">Home</a>
								</li>-->
								<li class="breadcrumb-item">
									<a href="#">Import E-Stamp</a>
								</li>
								<li class="breadcrumb-item active text-gray" aria-current="page">Upload File</li>
								
							</ol>
						</nav><?php */?>
						
						<div class="top-right-button-container">
							<a href="setup-company-view2.php" style="min-width: 90px;" class="btn btn-outline-primary btn-sm rounded-05 mr-2">Cancle</a>
							<a href="javascript:;" data-toggle="modal" data-target="#saveAlertconfirm" style="min-width: 90px;" class="btn btn-primary btn-sm rounded-05">Save</a>
						</div>

                    </div>
					
					
					
					<div class="card mb-3">
					<div class="card-body p-3">
						<div id="smart" class="">
						<div class="sw-main sw-theme-dots d-flex justify-content-between align-items-center">
                            <ul class="card-header nav nav-tabs step-anchor step-tabs" role="tablist">
                                <li class="nav-item ">
									<a id="first-tab" href="setup-company-create.php" class="nav-link text">
									<i>1</i>
									<span>General Profile</span>
									</a>
								</li>
                                <li class="nav-item active">
									<a id="second-tab" href="setup-company-create2.php" class="nav-link text">
									<i>2</i>
									<span>Branch Profile</span>
									</a>
								</li>
								 <li class="nav-item">
									<a id="second-tab" href="setup-company-create3.php" class="nav-link text">
									<i>3</i>
									<span>Email Server</span>
									</a>
								</li>
								 <li class="nav-item">
									<a id="second-tab" href="setup-company-create4.php" class="nav-link text">
									<i>4</i>
									<span>Config CA</span>
									</a>
								</li>
								<!--<li class="fix-r">
										
								</li>-->
                            </ul>
							<div class="top-right-button-container">
											<a class="btn btn-success btn-md top-right-button rounded-05" href="javascript:;" data-toggle="modal" data-target="#addBranch" style="min-width: 120px"> <i class="icon-img"><img src="di/ic-plus.png" height="20"></i> Branch</a>
										</div>
							</div>


                            <div class="card-body">
                                <div id="dotStep1" class="tab-pane show active" role="tabpanel" aria-labelledby="first-tab">
      
                                   	
									<div class="dc-add-tb dc-min-h ds-table mt-4">
										<table class="table table-striped table-responsive-xs">
												<thead>
													<tr>
														<th scope="col">No</th>
														<th scope="col">Branch name</th>
														<th scope="col">Branch Code</th>
														<th scope="col">Address</th>
														<th scope="col">Action</th>
													</tr>
												</thead>
												<tbody>
													<?php for($i=1;$i<=8;$i++){ ?>
													<tr class="tb-list">
														<td valign="middle" class="text-left"><?php echo $i; ?></td>
														<td valign="middle" class="text-left">สาขาย่อยหลัก 4</td>
														<td valign="middle" class="text-left"><a href="setup-company-view.php">00001</a></td>
														<td valign="middle" class="text-left"><a href="setup-company-edit.php">1693 จตุจักร จตุจักร กรุงเทพมหานคร 10900</a></td>
														<td valign="middle" class="text-left">

															<a href="javascript:;" data-toggle="modal" data-target="#editBranch" class="btn btn-xs p-1" title="View"><i class="icon-img"><img src="di/ic-edit-setup.png" height="16"></i></a>
															<a href="javascript:;" data-dismiss="modal" data-toggle="modal" data-target="#removeModalconfirm" class="btn  btn-xs p-1" title="View"><i class="icon-img"><img src="di/ic-trash.png" height="16"></i></a>
														</td>



													</tr>
													<?php } ?>



												</tbody>
											</table>
									</div>



									<div class="ft-paging d-flex justify-content-between align-items-center">
										<div class="dropdown-as-select display-page" id="pageCount">
											<span class="text-black text-small">1-10 of 195 items</span>
										</div>
										<div class="d-block d-md-inline-block ml-5">
											<nav class="ctrl-page d-flex flex-nowrap align-items-center">
												<ul class="pagination justify-content-center mb-0">
												   <!-- <li class="page-item ">
														<a class="page-link first" href="#">
															<i class="simple-icon-control-start"></i>
														</a>
													</li>-->
													<li class="page-item ">
														<a class="page-link prev" href="#">
															<i class="simple-icon-arrow-left"></i>
														</a>
													</li>
													<li class="page-item active">
														<a class="page-link" href="#">1</a>
													</li>
													<li class="page-item ">
														<a class="page-link" href="#">2</a>
													</li>
													<li class="page-item">
														<a class="page-link" href="#">3</a>
													</li>
													<li class="page-item">
														<a class="page-link" href="#">4</a>
													</li>
													<li class="page-item">
														<a class="page-link" href="#">5</a>
													</li>
													<li class="page-item">
														<span class="page-link">...</span>
													</li>
													<li class="page-item">
														<a class="page-link" href="#">19</a>
													</li>
													<li class="page-item ">
														<a class="page-link next" href="#" aria-label="Next">
															<i class="simple-icon-arrow-right"></i>
														</a>
													</li>
													<!--<li class="page-item ">
														<a class="page-link last" href="#">
															<i class="simple-icon-control-end"></i>
														</a>
													</li>-->
												</ul>

													<button class="btn btn-outline-dark btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
														12
													</button>
													<div class="dropdown-menu dropdown-menu-right">
														<a class="dropdown-item" href="#">5</a>
														<a class="dropdown-item active" href="#">12</a>
														<a class="dropdown-item" href="#">24</a>
													</div>
											</nav>
										</div>


									</div>


									
									<div class="ctrl-btn d-flex justify-content-end mt-5">
										
										<div class="top-right-button-container">
										<a href="setup-company-create.php" class="btn btn-outline-primary btn-lg rounded-05 mr-2">Previous</a>
										<a href="setup-company-create3.php" class="btn btn-primary btn-lg rounded-05">Next</a>
										</div>
									</div>
									
                                </div>
                            </div>
                        </div>

							
					</div>
					
	
					
					</div>
					
					<!-- end -->

                </div>
            </div>
        </div>

    </main>
<!-- Modal -->
    <div class="modal fade" id="addBranch" tabindex="-1" role="dialog"	aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header pb-0 border-0">

				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body pt-0 pb-3 text-center">
			  

				<div class="row">
					<div class="col-12 mb-3">
						<label class="form-group mb-0 has-float-label">
								<input type="text" class="input-sm form-control rounded-05" placeholder="iRich Co.ltd.,">
							<span>Company name *</span>
						</label>
					</div>
					<div class="col-6 mb-3">
						<label class="form-group w-100 mb-0">
								<input type="text" class="input-sm form-control rounded-05" placeholder="Branch Name *">
						</label>
					</div>
					<div class="col-6 mb-3">
						<label class="form-group w-100 mb-0">
								<input type="text" class="input-sm form-control rounded-05" placeholder="Branch Code*">
						</label>
					</div>

					<div class="col-6 mb-3">
						<label class="form-group w-100 mb-0">
							<input type="text" class="input-sm form-control rounded-05" placeholder="Buiding Number *">
						</label>
					</div>
					<div class="col-6 mb-3">
						<label class="form-group w-100 mb-0">
								<input type="text" class="input-sm form-control rounded-05" placeholder="Building Name">
						</label>
					</div>

					<div class="col-12 mb-3">
						<label class="form-group w-100 mb-0">
							<textarea class="form-control rounded-05" rows="2" name="jQueryDetail" required="" placeholder="Address *"></textarea>
						</label>
					</div>

					<div class="col-6 mb-3">
						<label class="form-group w-100 mb-0">
								<input type="text" class="input-sm form-control rounded-05" placeholder="Sub District *">
						</label>
					</div>
					<div class="col-6 mb-3">
						<label class="form-group w-100 mb-0">
								<input type="text" class="input-sm form-control rounded-05" placeholder="District *">
						</label>
					</div>

					<div class="col-6 mb-3">
						<label class="form-group w-100 mb-0">
								<input type="text" class="input-sm form-control rounded-05" placeholder="Province *">
						</label>
					</div>
					<div class="col-6 mb-3">
						<label class="form-group w-100 mb-0">
								<input type="text" class="input-sm form-control rounded-05" placeholder="Pose code *">
						</label>
					</div>

					<div class="col-6 mb-3">
						<label class="form-group w-100 mb-0">
								<input type="text" class="input-sm form-control rounded-05" placeholder="Email *">
						</label>
					</div>
					<div class="col-6 mb-3">
						<label class="form-group w-100 mb-0">
								<input type="text" class="input-sm form-control rounded-05" placeholder="Telephone number *">
						</label>
					</div>
					<div class="col-12 mb-3">
						<label class="form-group w-100 mb-3l">
								<input type="text" class="input-sm form-control rounded-05" placeholder="Fax *">
						</label>
					</div>
				</div>
			  
			</div>
			<div class="modal-footer pt-3 d-flex justify-content-cenendter border-0">
				
				<button type="button" class="btn btn-md btn-outline-primary rounded-05 col-3" data-dismiss="modal">Cancel</button>	
				<button type="button" class="btn btn-md btn-primary rounded-05 col-3" data-dismiss="modal" data-toggle="modal" data-target="#SaveCPNModalsuccess">Confirm</button>
			</div>

		</div>
	</div>
</div>

<div class="modal fade" id="editBranch" tabindex="-1" role="dialog"	aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header pb-0 border-0">

				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body pt-0 pb-3 text-center">
			  

				<div class="row">
					<div class="col-12 mb-3">
						<label class="form-group mb-1 has-float-label">
								<input type="text" class="input-sm form-control rounded-05 bg-white text-gray" value="iRich Co.ltd.," readonly>
							<span>Company name *</span>
						</label>
					</div>
					<div class="col-6 mb-3">
						<label class="form-group mb-0 has-float-label">
								<input type="text" class="input-sm form-control rounded-05" value="9999999999">
							<span>Branch Name *</span>
						</label>
					</div>
					<div class="col-6 mb-3">
						<label class="form-group mb-0 has-float-label">
								<input type="text" class="input-sm form-control rounded-05" value="00001">
								<span>Branch Code *</span>
						</label>
					</div>

					<div class="col-6 mb-3">
						<label class="form-group mb-0 has-float-label">
								<input type="text" class="input-sm form-control rounded-05" value="1683">
							<span>Buiding Number *</span>
						</label>
					</div>
					<div class="col-6 mb-3">
						<label class="form-group mb-0 input-group">
								<input type="text" class="input-sm form-control rounded-05" value="Building name">
								<!--<span class="input-group-text input-group-append input-group-addon border-left-0">
									<i class="simple-icon-magnifier"></i>
								</span>-->
						</label>
					</div>

					<div class="col-12 mb-3">
						<label class="form-group mb-0 has-float-label">
							<textarea class="form-control rounded-05" rows="2" name="jQueryDetail" required="">ถนนพหลโยธิน เซ็นทรัลพลาซ่าลาดพร้าว 1</textarea>
							<span>Address *</span>
						</label>
					</div>

					<div class="col-6 mb-3">
						<label class="form-group mb-0 has-float-label">
								<input type="text" class="input-sm form-control rounded-05" value="จตุจักร">
							<span>Sub District *</span>
						</label>
					</div>
					<div class="col-6 mb-3">
						<label class="form-group mb-0 has-float-label">
								<input type="text" class="input-sm form-control rounded-05" value="จตุจักร">
							<span>District *</span>
						</label>
					</div>

					<div class="col-6 mb-3">
						<label class="form-group mb-0 has-float-label">
								<input type="text" class="input-sm form-control rounded-05" value="กรุงเทพมหานคร">
							<span>Province *</span>
						</label>
					</div>
					<div class="col-6 mb-3">
						<label class="form-group mb-0 has-float-label">
								<input type="text" class="input-sm form-control rounded-05" value="10900">
							<span>Pose code *</span>
						</label>
					</div>

					<div class="col-6 mb-3">
						<label class="form-group mb-0 has-float-label">
								<input type="text" class="input-sm form-control rounded-05" value="etaxpoc@gmail.com">
							<span>Email *</span>
						</label>
					</div>
					<div class="col-6 mb-3">
						<label class="form-group mb-0 has-float-label">
								<input type="text" class="input-sm form-control rounded-05" value="+6629999999">
							<span>Telephone number *</span>
						</label>
					</div>
					<div class="col-12 mb-3">
						<label class="form-group mb-3 has-float-label">
								<input type="text" class="input-sm form-control rounded-05" value="+6629999999">
							<span>Fax *</span>
						</label>
					</div>
				</div>
			  
			</div>
			<div class="modal-footer pt-3 d-flex justify-content-cenendter border-0">
				
				<button type="button" class="btn btn-md btn-outline-primary rounded-05 col-3" data-dismiss="modal">Cancel</button>	
				<button type="button" class="btn btn-md btn-primary rounded-05 col-3" data-dismiss="modal" data-toggle="modal" data-target="#SaveCPNModalsuccess">Confirm</button>
			</div>

		</div>
	</div>
</div>
<!-- /Modal -->    

    <?php include("incs/js.html") ?>
	<?php include("incs/modal.html") ?>


	
	<script src="js/vendor/jquery.smartWizard.min.js"></script>
    <script src="js/vendor/bootstrap-datepicker.js"></script>
    <script src="js/dore.script.js"></script>
    <script src="js/scripts.js"></script>
	<script>
	$(document).ready(function() {
	   $('.select2-normal').select2({
			//placeholder: 'Content Language',
			minimumResultsForSearch: -1,
			//width: 350
		});
	} );
	</script>
<script>
$(document).ready(function() {
	$('.main-menu>.scroll>.list-unstyled>li>a.rotate-arrow-icon').addClass('collapsed');
	$('.main-menu>.scroll>.list-unstyled>li>.collapse').removeClass('show');
	$('.main-menu .inner-level-menu>li').removeClass('active');
	$('.main-menu>.scroll>.list-unstyled>li:nth-child(8)>a.rotate-arrow-icon').removeClass('collapsed');
	$('.main-menu>.scroll>.list-unstyled>li:nth-child(8)>.collapse').addClass('show');
	$('.main-menu>.scroll>.list-unstyled>li:nth-child(8) .inner-level-menu>li:nth-child(1)').addClass('active');
});
</script>
</body>

</html>