<!DOCTYPE html>
<html lang="en">

<!-- Top Head -->
<?php include("incs/header-top.html") ?>
<!-- /Top Head -->

<body id="app-container" class="menu-sub-hidden show-spinner">
    <?php include("incs/header.html") ?>
    <?php include("incs/sidebar-left.html") ?>

    <main>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 mb-2">

                        <h1 class="text-primary">Document Form Template</h1>
						
						<?php /*?><nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
							<ol class="breadcrumb pt-0">
								<!--<li class="breadcrumb-item">
									<a href="#">Home</a>
								</li>-->
								<li class="breadcrumb-item">
									<a href="#">Import E-Stamp</a>
								</li>
								<li class="breadcrumb-item active text-gray" aria-current="page">Upload File</li>
								
							</ol>
						</nav><?php */?>

				</div>
			</div>
			
			<div class="row">
					<aside class="menu tmp-menu">
						<div class="sub-menu">
							<h2 class="bg-primary pt-3 pb-3 pl-4 pr-4 h6">List all template</h2>
							<ul class="list-unstyled inner-level-menu">
									<li class="active"><a href="#"><span class="d-block">Invoice/Tax Invoice</span></a></li>
									<li><a href="#"><span class="d-block">Receipt/Tax Invoice</span></a></li>
									<li><a href="#"><span class="d-block">Delivery Order/Tax Invoice</span></a></li>
									<li><a href="#"><span class="d-block">Tax Invoice</span></a></li>
									<li><a href="#"><span class="d-block">Debit Note</span></a></li>

									<li><a href="#"><span class="d-block">Credit Note</span></a></li>
									<li><a href="#"><span class="d-block">Cancelation Note</span></a></li>
									<li><a href="#"><span class="d-block">Abbreviated Invoic</span></a></li>
									<li><a href="#"><span class="d-block">Receipt</span></a></li>
									<li><a href="#"><span class="d-block">Invoice</span></a></li>

									<li><a href="#"><span class="d-block">Quotation</span></a></li>
									<!--<li><a href="#"><span class="d-block">Document List (Sell)</span></a></li>
									<li><a href="#"><span class="d-block">Document List (Purchase)</span></a></li>
									<li><a href="#"><span class="d-block">Generate PDF</span></a></li>
									<li><a href="#"><span class="d-block">Generate XML</span></a></li>	

									<li><a href="#"><span class="d-block">Download Document</span></a></li>		
									<li><a href="#"><span class="d-block">Remove Document</span></a></li>	-->	
								</ul>
						</div>
					</aside>

					<div class="col">	

						<div class="card mb-3">
						<div class="card-body p-3">
								<div class="search-bar">
								<a class="btn p-2 d-inline-block d-md-none" data-toggle="collapse" href="#searchOptions" role="button" aria-expanded="true" aria-controls="searchOptions">
									Display Options
									<i class="simple-icon-arrow-down align-middle"></i>
								</a>
								<div id="searchOptions">
									<div class="d-flex flex-wrap row align-items-center">
										<div class="col-12 col-sm input-group flex-nowrap mb-0">								

											<h2 class="h6 mb-2 mt-2">Invoice/Tax invoice</h2>
										</div>


										<div class="top-right-button-container text-nowrap col-12 col-sm-auto mb-2">

													<a class="btn btn-success btn-md top-right-button rounded-05" href="javascript:;" data-toggle="modal" data-target="#addDocTMPModal" style="min-width: 120px"><i class="icon-img f-white mt-1n"><img src="di/ic-plus.png" height="16"></i> Add</a>
										</div>
									</div>
								</div>
								</div>



								<div class="dc-add-tb dc-min-h ds-table mt-4">
									<table class="table table-striped table-responsive-xs">
											<thead>
												<tr>
													<th scope="col">No</th>
													<th scope="col">Template name</th>
													<th scope="col">Default flag</th>
													<th scope="col">Created date</th>
													<th scope="col">Last updated date</th>
													<th scope="col" class="text-center">Action</th>
												</tr>
											</thead>
											<tbody>
												<?php for($i=1;$i<=10;$i++){ ?>
												<tr class="tb-list">
													<td valign="middle" class="text-left"><?php echo $i; ?></td>

													<td valign="middle" class="text-left">
														<?php if($i%2==0){ ?>custTemplate04
														<?php } elseif($i%3==0){ ?>Sumet Template
														<?php } elseif($i%5==0){ ?>New Invoice Template
														<?php } elseif($i%7==0){ ?>SomkietTemplate
														<?php } else { ?>Invoice/ Tax
														<?php }?>
													</td>

													<td class="text-left" valign="middle">
														<label class="custom-control custom-checkbox mb-0 mr-n3 ml-4 d-inline-block">
															<input type="checkbox" class="custom-control-input" checked="">
															<span class="custom-control-label">&nbsp;</span>
														</label>
													</td>
													<td valign="middle" class="text-left">26/08/2020 11:23</td>
													<td class="text-left" valign="middle">26/08/2020 11:23</td>
													<td class="text-center" valign="middle">
														<a href="javascript:;" data-toggle="modal" data-target="#editDocTMPModal" class="btn btn-xs p-1" title="View"><i class="icon-img"><img src="di/ic-edit-setup.png" height="16"></i></a>
														<a href="javascript:;" data-dismiss="modal" data-toggle="modal" data-target="#removeModalconfirm" class="btn  btn-xs p-1" title="View"><i class="icon-img"><img src="di/ic-trash.png" height="16"></i></a>
													</td>




												</tr>
												<?php } ?>



											</tbody>
										</table>
								</div>



								<div class="ft-paging d-flex justify-content-between align-items-center">
									<div class="dropdown-as-select display-page" id="pageCount">
										<span class="text-black text-small">1-10 of 195 items</span>
									</div>
									<div class="d-block d-md-inline-block ml-5">
										<nav class="ctrl-page d-flex flex-nowrap align-items-center">
											<ul class="pagination justify-content-center mb-0">
											   <!-- <li class="page-item ">
													<a class="page-link first" href="#">
														<i class="simple-icon-control-start"></i>
													</a>
												</li>-->
												<li class="page-item ">
													<a class="page-link prev" href="#">
														<i class="simple-icon-arrow-left"></i>
													</a>
												</li>
												<li class="page-item active">
													<a class="page-link" href="#">1</a>
												</li>
												<li class="page-item ">
													<a class="page-link" href="#">2</a>
												</li>
												<li class="page-item">
													<a class="page-link" href="#">3</a>
												</li>
												<li class="page-item">
													<a class="page-link" href="#">4</a>
												</li>
												<li class="page-item">
													<a class="page-link" href="#">5</a>
												</li>
												<li class="page-item">
													<span class="page-link">...</span>
												</li>
												<li class="page-item">
													<a class="page-link" href="#">19</a>
												</li>
												<li class="page-item ">
													<a class="page-link next" href="#" aria-label="Next">
														<i class="simple-icon-arrow-right"></i>
													</a>
												</li>
												<!--<li class="page-item ">
													<a class="page-link last" href="#">
														<i class="simple-icon-control-end"></i>
													</a>
												</li>-->
											</ul>

												<button class="btn btn-outline-dark btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
													12
												</button>
												<div class="dropdown-menu dropdown-menu-right">
													<a class="dropdown-item" href="#">5</a>
													<a class="dropdown-item active" href="#">12</a>
													<a class="dropdown-item" href="#">24</a>
												</div>
										</nav>
									</div>
								</div>


						</div>
						</div>

						<!-- end -->
					</div>
                </div>

        </div>
    </main>

    <!-- Modal -->
<div class="modal fade" id="addDocTMPModal" tabindex="-1" role="dialog"	aria-hidden="true">
	<div class="modal-dialog modal-xl" role="document">
		<div class="modal-content">
			<div class="modal-header mt-n2 pb-0 border-0">

				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body pt-2 pb-0">
			   <div class="row">
			   		
					<div class="col-6 mb-3">
						<label class="form-group w-100 mb-0">
								<input type="text" class="input-sm form-control rounded-05" placeholder="Template name *">
						</label>
					</div>
					<div class="col-6 mb-3">
						<div class="custom-control custom-checkbox pt-2 d-inline-block">
							<input type="checkbox" class="custom-control-input" id="jQueryCustomCheck1" name="jQueryCheckbox">
							<label class="custom-control-label" for="jQueryCustomCheck1">Default flag</label>
						</div>
					</div>
			   </div>
			   
			   <div class="row">
					<div class="col-12 mb-3" id="editor">
						<img src="di/editor-tmp-doc1.jpg" class="d-block w-100">
					</div>
			   </div>
			   
			</div>
			<div class="modal-footer pt-2 d-flex justify-content-end border-0">
				<button type="button" class="btn btn-md btn-outline-primary rounded-05" style="min-width: 120px" data-dismiss="modal">Preview</button>
				<button type="button" class="btn btn-md btn-gray rounded-05 disabled" style="min-width: 120px"  data-dismiss="modal" data-toggle="modal" data-target="#saveTMPAlertconfirm">Save</button>			
				<!--<button type="button" class="btn btn-md btn-primary rounded-05 col-3" data-dismiss="modal" data-toggle="modal" data-target="#SaveCPNModalsuccess">Save</button>		-->	
			</div>
		</div>
	</div>
</div>


<div class="modal fade" id="editDocTMPModal" tabindex="-1" role="dialog"	aria-hidden="true">
	<div class="modal-dialog modal-xl" role="document">
		<div class="modal-content">
			<div class="modal-header mt-n2 pb-0 border-0">

				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body pt-2 pb-0">
			   <div class="row">
			   		<div class="col-6 mb-3">
						<label class="form-group mb-0 has-float-label">
								<input type="text" class="input-sm form-control rounded-05" value="SomkietTemplate">
								<span>Template Name *</span>
						</label>
					</div>
					<div class="col-6 mb-3">
						<div class="custom-control custom-checkbox pt-2 d-inline-block">
							<input type="checkbox" class="custom-control-input" id="jQueryCustomCheck1" name="jQueryCheckbox" checked>
							<label class="custom-control-label" for="jQueryCustomCheck1">Default flag</label>
						</div>
					</div>
			   </div>
			   
			    <div class="row">
					<div class="col-12 mb-3" id="editor">
						<img src="di/editor-tmp-doc2.jpg" class="d-block w-100">
					</div>
			   </div>
			   
			</div>
			<div class="modal-footer pt-2 d-flex justify-content-end border-0">
				<button type="button" class="btn btn-md btn-outline-primary rounded-05" style="min-width: 120px"  data-dismiss="modal">Preview</button>
				<button type="button" class="btn btn-md btn-primary rounded-05" style="min-width: 120px"  data-dismiss="modal" data-toggle="modal" data-target="#saveTMPAlertconfirm">Save</button>				
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="saveTMPAlertconfirm" tabindex="-1" role="dialog"	aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header pb-0 border-0">

				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body p-0 text-center">
			   <i class="icon-img"><img src="di/ic-warning-orange.png" height="100"></i>
			   <h5 class="text-dark mt-3">Save document template</h5>
				<p>Would you like to save document template?</p>
			</div>
			<div class="modal-footer pt-2 d-flex justify-content-center border-0">
				<button type="button" class="btn btn-md btn-success rounded-05 col-3" data-dismiss="modal" data-toggle="modal" data-target="#saveModalsuccess">Confirm</button>
				<button type="button" class="btn btn-md btn-red rounded-05 col-3" data-dismiss="modal">Cancel</button>				
			</div>
		</div>
	</div>
</div>
<!-- /Modal -->    

    <?php include("incs/js.html") ?>
	<?php include("incs/modal.html") ?>
	
	
    <script src="js/vendor/bootstrap-datepicker.js"></script>
    <script src="js/dore.script.js"></script>
    <script src="js/scripts.js"></script>
	<script>
	$(document).ready(function() {
	   $('.select2-normal').select2({
			//placeholder: 'Content Language',
			minimumResultsForSearch: -1,
			//width: 350
		});
	} );
	</script>
<script>
$(document).ready(function() {
	$('.main-menu>.scroll>.list-unstyled>li>a.rotate-arrow-icon').addClass('collapsed');
	$('.main-menu>.scroll>.list-unstyled>li>.collapse').removeClass('show');
	$('.main-menu .inner-level-menu>li').removeClass('active');
	$('.main-menu>.scroll>.list-unstyled>li:nth-child(7)>a.rotate-arrow-icon').removeClass('collapsed');
	$('.main-menu>.scroll>.list-unstyled>li:nth-child(7)>.collapse').addClass('show');
	$('.main-menu>.scroll>.list-unstyled>li:nth-child(7) .inner-level-menu>li:nth-child(1)').addClass('active');
});
</script>
</body>

</html>