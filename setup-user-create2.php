<!DOCTYPE html>
<html lang="en">

<!-- Top Head -->
<?php include("incs/header-top.html") ?>
<!-- /Top Head -->

<body id="app-container" class="menu-sub-hidden show-spinner">
    <?php include("incs/header.html") ?>
    <?php include("incs/sidebar-left.html") ?>

    <main>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="mb-2">
                        <h1 class="text-primary">Create User</h1>
						
						<?php /*?><nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
							<ol class="breadcrumb pt-0">
								<!--<li class="breadcrumb-item">
									<a href="#">Home</a>
								</li>-->
								<li class="breadcrumb-item">
									<a href="#">Import E-Stamp</a>
								</li>
								<li class="breadcrumb-item active text-gray" aria-current="page">Upload File</li>
								
							</ol>
						</nav><?php */?>
						
						<div class="top-right-button-container">
							<a href="setup-role.php" style="min-width: 90px;" class="btn btn-outline-primary btn-sm rounded-05 mr-2">Cancle</a>
							<a href="setup-role.php" style="min-width: 90px;" class="btn btn-primary btn-sm rounded-05">Save</a>
						</div>

                    </div>
					
					
					
					<div class="card mb-3">
					<div class="card-body p-3">
						<div id="smart" class="sw-main sw-theme-dots">
                            <ul class="card-header nav nav-tabs step-anchor step-tabs" role="tablist">
                                <li class="nav-item">
									<a id="first-tab" href="setup-user-create.php" class="nav-link text">
									<i>1</i>
									<span>User Profile</span>
									</a>
								</li>
                                <li class="nav-item active">
									<a id="second-tab" href="setup-user-create2.php" class="nav-link text">
									<i>2</i>
									<span>Set Company</span>
									</a>
								</li>
								 <li class="nav-item">
									<a id="second-tab" href="setup-user-create3.php" class="nav-link text">
									<i>3</i>
									<span>Set Branch</span>
									</a>
								</li>
								 <li class="nav-item">
									<a id="second-tab" href="setup-user-create4.php" class="nav-link text">
									<i>4</i>
									<span>Set Role</span>
									</a>
								</li>
                            </ul>
							


                            <div class="card-body">
                                <div id="dotStep1" class="tab-pane show active" role="tabpanel" aria-labelledby="first-tab">
      
                                   
									<div class="dc-add-tb dc-min-h ds-table mt-4">
										<table class="table table-striped table-responsive-xs">
												<thead>
													<tr>
														<th scope="col">No</th>
														<th scope="col">Campany</th>
														<th scope="col" width="35%">
															<label class="custom-control custom-checkbox mb-0 mr-n3 d-inline-block">
																<input type="checkbox" class="custom-control-input">
																<span class="custom-control-label">&nbsp;</span>
															</label>
														</th>
													</tr>
												</thead>
												<tbody>
													<?php for($i=1;$i<=10;$i++){ ?>
													<tr class="tb-list">
														<td valign="middle" class="text-left">
															<?php echo $i; ?>
														</td>
														<td valign="middle" class="text-left">

															<?php if($i==1){ ?>
																iRich Co.ltd.,
															<?php } elseif($i==2){ ?>
															 Arisara Company
															<?php } elseif($i==3){ ?>
															บริษัท เคซีจี คอร์ปอเรชั่น จำกัด
															<?php } elseif($i==4){ ?>
															บริษัท เอ็นโด้ ไล้ท์ติ้ง (ประเทศไทย) จำกัด (มหาชน)
															<?php } elseif($i==5){ ?>
															 etax co., Ltd.
															 <?php } elseif($i==6){ ?>
															  บริษัท เปเปอร์เมท (ประเทศไทย) จำกัด
															 <?php } elseif($i==7){ ?>
															 iRich Co.ltd.,
															  <?php } elseif($i==8){ ?>
															 Arisara Company
															  <?php } elseif($i==9){ ?>
															 บริษัท เคซีจี คอร์ปอเรชั่น จำกัด
				
															<?php } else { ?>
																บริษัท เอ็นโด้ ไล้ท์ติ้ง (ประเทศไทย) จำกัด (มหาชน)
															<?php } ?>
														</td>
														
														<td valign="middle" class="text-left">
															<label class="custom-control custom-checkbox mb-0 mr-n3 d-inline-block">
																<input type="checkbox" class="custom-control-input" checked>
																<span class="custom-control-label">&nbsp;</span>
															</label>
														</td>



													</tr>
													<?php } ?>

												</tbody>
											</table>
											
											
									</div>
									
										
									<div class="ft-paging d-flex justify-content-between align-items-center">
										<div class="dropdown-as-select display-page" id="pageCount">
											<span class="text-black text-small">1-10 of 195 items</span>
										</div>
										<div class="d-block d-md-inline-block ml-5">
											<nav class="ctrl-page d-flex flex-nowrap align-items-center">
												<ul class="pagination justify-content-center mb-0">
												   <!-- <li class="page-item ">
														<a class="page-link first" href="#">
															<i class="simple-icon-control-start"></i>
														</a>
													</li>-->
													<li class="page-item ">
														<a class="page-link prev" href="#">
															<i class="simple-icon-arrow-left"></i>
														</a>
													</li>
													<li class="page-item active">
														<a class="page-link" href="#">1</a>
													</li>
													<li class="page-item ">
														<a class="page-link" href="#">2</a>
													</li>
													<li class="page-item">
														<a class="page-link" href="#">3</a>
													</li>
													<li class="page-item">
														<a class="page-link" href="#">4</a>
													</li>
													<li class="page-item">
														<a class="page-link" href="#">5</a>
													</li>
													<li class="page-item">
														<span class="page-link">...</span>
													</li>
													<li class="page-item">
														<a class="page-link" href="#">19</a>
													</li>
													<li class="page-item ">
														<a class="page-link next" href="#" aria-label="Next">
															<i class="simple-icon-arrow-right"></i>
														</a>
													</li>
													<!--<li class="page-item ">
														<a class="page-link last" href="#">
															<i class="simple-icon-control-end"></i>
														</a>
													</li>-->
												</ul>

													<button class="btn btn-outline-dark btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
														12
													</button>
													<div class="dropdown-menu dropdown-menu-right">
														<a class="dropdown-item" href="#">5</a>
														<a class="dropdown-item active" href="#">12</a>
														<a class="dropdown-item" href="#">24</a>
													</div>
											</nav>
										</div>


									</div>



									
									<div class="ctrl-btn d-flex justify-content-end clearfix mt-5">
										<a href="setup-user-create.php" class="btn btn-outline-primary btn-lg rounded-05 mr-2">Previous</a>
										<a href="setup-user-create3.php" class="btn btn-primary btn-lg rounded-05">Next</a>
									</div>
									
                                </div>
								
                            </div>
                        </div>

							
					</div>
					
	
					
					</div>
					
					<!-- end -->

                </div>
            </div>
        </div>

    </main>

    

    <?php include("incs/js.html") ?>
	<?php include("incs/modal.html") ?>


	
	<script src="js/vendor/jquery.smartWizard.min.js"></script>
    <script src="js/vendor/bootstrap-datepicker.js"></script>
    <script src="js/dore.script.js"></script>
    <script src="js/scripts.js"></script>
	<script>
	$(document).ready(function() {
	   $('.select2-normal').select2({
			//placeholder: 'Content Language',
			minimumResultsForSearch: -1,
			//width: 350
		});
	} );
	</script>
<script>
$(document).ready(function() {
	$('.main-menu>.scroll>.list-unstyled>li>a.rotate-arrow-icon').addClass('collapsed');
	$('.main-menu>.scroll>.list-unstyled>li>.collapse').removeClass('show');
	$('.main-menu .inner-level-menu>li').removeClass('active');
	$('.main-menu>.scroll>.list-unstyled>li:nth-child(9)>a.rotate-arrow-icon').removeClass('collapsed');
	$('.main-menu>.scroll>.list-unstyled>li:nth-child(9)>.collapse').addClass('show');
	$('.main-menu>.scroll>.list-unstyled>li:nth-child(9) .inner-level-menu>li:nth-child(1)').addClass('active');
});
</script>
</body>

</html>