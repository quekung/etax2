<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Dore jQuery</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<link href="https://fonts.googleapis.com/css?family=Sarabun:300,400,600,700" rel="stylesheet"> 
    <link rel="stylesheet" href="font/iconsmind-s/css/iconsminds.css" />
    <link rel="stylesheet" href="font/simple-line-icons/css/simple-line-icons.css" />

    <link rel="stylesheet" href="css/vendor/bootstrap.min.css" />
    <link rel="stylesheet" href="css/vendor/bootstrap.rtl.only.min.css" />
    <link rel="stylesheet" href="css/vendor/bootstrap-float-label.min.css" />
	<link rel="stylesheet" href="font/fontface.css" />
    <link rel="stylesheet" href="css/main.css" />
</head>

<body class="background show-spinner no-footer">
    <div class="fixed-background image"></div>
    <main>
        <div class="container">
            <div class="row h-100">
                <div class="col-12 col-md-10 mx-auto my-auto">
                    <div class="card auth-card">

                        <div class="form-side rounded-1 d-flex justify-content-center p-3">
						<div class="inner col-12 col-sm-10 col-lg-8">
                            <a class="d-flex justify-content-center mt-2 mb-5" href="Dashboard.Default.html">
                                <img src="di/logo-client.png" height="90">
                            </a>
 
                            <form id="form-client" method="post" action="client-doc-list.php">
                                <div class="form-group mb-4">
                                    <label for="id-client" class="mb-2 text-primary h6">หมายเลขบริการ หรือ รหัสผู้ใช้งาน</label>
									<input class="form-control rounded-05 form-control-lg" id="id-client" name="id-client" placeholder="โปรดระบุ" />
                                </div>

                                <div class="form-group mb-4">
                                    <label for="mobile-otp" class="mb-2 text-primary h6">หมายเลขโทรศัพท์ที่ใช้รับรหัส OTP</label>
									<div class="d-flex">
										<div class="col pl-0"><input class="form-control rounded-05 form-control-lg" id="mobile-otp" name="mobile-otp" placeholder="โปรดระบุ" /></div>
										<button class="btn btn-blue rounded-05" id="getOTP">ขอรหัส OTP</button>
									</div>
									<p class="text-danger mt-2">* หากไม่ได้รับรหัส OTP  ภายใน 1 นาที ให้กดขอรหัส OTP ใหม่อีกครั้ง</p>
                                </div>
								
								<div class="form-group mb-4">
                                    <label for="code-otp" class="mb-2 text-primary h6">กรอกรหัส OTP</label>
									<div class="d-flex">
										<div class="col-auto pl-0"><input class="form-control rounded-05 form-control-lg" id="code-otp" name="code-otp" placeholder="โปรดระบุ" /></div>
									</div>
									<!--<p class="text-danger mt-2">รหัส OTP ไม่ถูกต้อง</p>-->
                                </div>

                                <div class="d-flex justify-content-between align-items-center mb-4">
                                    
                                    <button class="btn btn-primary btn-block btn-lg btn-shadow rounded-05" type="submit">ยืนยัน</button>
                                </div>
                            </form>
							
						</div>
                        </div>
						
                    </div>
                </div>
            </div>
        </div>
    </main>
    <script src="js/vendor/jquery-3.3.1.min.js"></script>
    <script src="js/vendor/bootstrap.bundle.min.js"></script>
    <script src="js/dore.script.js"></script>
    <script src="js/scripts.js"></script>
</body>

</html>