<!DOCTYPE html>
<html lang="en">

<!-- Top Head -->
<?php include("incs/header-top.html") ?>
<!-- /Top Head -->

<body id="app-container" class="menu-sub-hidden show-spinner">
    <?php include("incs/header.html") ?>
    <?php include("incs/sidebar-left.html") ?>

    <main>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="mb-2">
                        <h1 class="text-primary">Tax Invoice (ใบกำกับภาษีอิเล็กทรอนิกส์)</h1>
						
						<?php /*?><nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
							<ol class="breadcrumb pt-0">
								<!--<li class="breadcrumb-item">
									<a href="#">Home</a>
								</li>-->
								<li class="breadcrumb-item">
									<a href="#">Import E-Stamp</a>
								</li>
								<li class="breadcrumb-item active text-gray" aria-current="page">Upload File</li>
								
							</ol>
						</nav><?php */?>

                    </div>
					
					
					
					<div class="card mb-3">
					<div class="card-body p-3">
							<div class="dc-header d-flex flex-row-reverse justify-content-between">
								<h2>Tax Invoice </h2>
								<div>
									<div class="branch mb-2">
										<div class="sv-select-line">
											<select class="form-control border-left-0 border-right-0 border-top-0 bg-transparent">
												<option>สำนักงานใหญ่ (00000)</option>
												<option>สาขาย่อย(00001)</option>
												<option>สาขาย่อย(00002)</option>
											</select>
										</div>
									</div>
									<div class="info text-black-50">
										Tax ID: 9999999999999<br>
1693 จตุจักร จตุจักร กรุงเทพมหานคร 10900<br>
Tel:+6629999999 Email:etaxpoc@gdl-technology.com<br>
									</div>
								</div>
							</div>
							
							<div class="dc-customer row mt-4">
								<div class="col-12 col-sm-5">
									<div class="form-group">
										<div class="d-flex justify-content-between mb-2">
											<label>Customer</label>
											<button type="button" data-toggle="modal" data-target="#addRemarkModal" title="QR" class="btn btn-blue btn-xs t-white ml-1 rounded-05">+ Add</button>
										</div>
										<textarea class="form-control rounded-05" rows="2" name="cusDetail"></textarea>
									</div>
								</div>
								<div class="col-12 col-sm-7">
									<div class="row mt-2">
										<div class="col-6 d-flex flex-nowrap p-0">
											<div class=" col">
											<label class="form-group has-float-label">
												<input class="form-control rounded-05" type="text" placeholder="MT77-777777-7777">
												<span>No</span>
											</label>
											</div>
											<div class="custom-control custom-checkbox mt-2 mr-2">
												<input type="checkbox" class="custom-control-input" id="customCheckThis">
												<label class="custom-control-label" for="customCheckThis"></label>
											</div>
										</div>
										<div class="col-6">
											<label class="form-group has-float-label">
												<input class="form-control rounded-05 datepicker" placeholder="">
												<span>Date <small class="text-danger">*</small></span>
											</label>
										</div>
									</div>
									<div class="row mt-1">
										<div class="col-12">
											<label class="form-group has-float-label">
												<select class="form-control select2-normal" data-width="100%">
													<option selected>Invoice /Tax invoice</option>
													<option>option</option>
													<option>option</option>
												</select>
												<!--<input class="form-control" type="text" value="Invoice /Tax invoice">-->
												<span>Rec dot Type</span>
											</label>
										</div>
									</div>
									<div class="row mt-1">
										<div class="col-6">
											<label class="form-group has-float-label">
												<input class="form-control rounded-05" type="text" value="text">
												<span>Rec dot no</span>
											</label>
										</div>
										<div class="col-6">
											<label class="form-group has-float-label">
												<input class="form-control rounded-05 datepicker" placeholder="">
												<span>Rec dot date </span>
											</label>
										</div>
									</div>
								</div>
							</div>
							
							<div class="dc-add-tb ds-table mt-4">
								<table class="table table-striped table-responsive-xs">
										<thead>
											<tr>
												<th scope="col" class="text-center">Item No</th>
												<th scope="col">Item Name</th>
												<th scope="col">Unit</th>
												<th scope="col">Unit Price</th>
												<th scope="col">Quantity</th>
												<th scope="col">Totle</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td class="text-center">&nbsp;</td>
												<td class="text-left"><input class="form-control border-left-0 border-right-0 border-top-0 bg-transparent"  type="text"></td>
												<td class="text-left"><input class="form-control border-left-0 border-right-0 border-top-0 bg-transparent"  type="text"></td>
												<td class="text-left"><input class="form-control border-left-0 border-right-0 border-top-0 bg-transparent"  type="text"></td>
												<td class="text-left"><input class="form-control border-left-0 border-right-0 border-top-0 bg-transparent"  type="text"></td>
												<td class="text-right"><button type="button" class="btn btn-blue btn-xs p-1 rounded-5 text-large" style="line-height: 13px">+</button></td>
												
												
											</tr>
											
											
											
										</tbody>
									</table>
							</div>
							
							<div class="separator mb-3"></div>
							
							<div class="dc-data-list">
								<ul class="list-inline">
									<li class="d-flex justify-content-between p-2">
										<div class="text text-black-50">Sub Total</div>
										<div class="value text-right text-black-50">0.00 THB</div>
									</li>
									<li class="d-flex justify-content-between align-items-center p-1 pl-2 pr-2">
										<div class="text text-black-50">Discount</div>
										<div class="value text-right text-black-50 d-flex flex-nowrap align-items-center justify-content-end"><input class="form-control rounded-05 col-xxl-5 col-md-7 mr-1 form-control-sm" type="text" value="0.00"> <span>THB</span></div>
									</li>
									<li class="d-flex justify-content-between p-2">
										<div class="text text-black-50">Tax Basis Total</div>
										<div class="value text-right text-black-50">0.00 THB</div>
									</li>
									<li class="d-flex justify-content-between align-items-center p-1 pl-2 pr-2">
										<div class="text text-black-50 d-flex flex-nowrap align-items-center justify-content-end">
											<span class="mr-1">Tax Code</span>
											<select class="form-control select2-normal " data-width="70">
												<option selected>Vat</option>
												<option>option</option>
											</select>
											<span class="ml-1 mr-1">Tax</span>
											<select class="form-control select2-normal" data-width="50">
												<option selected>7</option>
												<option>5</option>
												<option>3</option>
											</select>
											<span class="ml-1 mr-1">%</span>
											<select class="form-control select2-normal" data-width="100">
												<option selected>Exclude Vat</option>
												<option>option</option>
											</select>
										</div>
										<div class="value text-right text-black-50">0.00 THB</div>
									</li>
									<li class="d-flex justify-content-between p-2">
										<div class="text text-black font-weight-bold">Total</div>
										<div class="value text-right text-black font-weight-bold">0.00 THB</div>
									</li>
									<li class="d-flex justify-content-between align-items-center p-1 pl-2 pr-2">
										<div class="text text-black-50 d-flex flex-nowrap align-items-center justify-content-end">
											
										<span class="mr-1">Withholding Tax</span>
											<select class="form-control select2-normal" data-width="50">
												<option selected>7</option>
												<option>5</option>
												<option>3</option>
											</select>
											<span class="ml-1 mr-1">%</span>
										</div>
										<div class="value text-right text-black-50">0.00 THB</div>
									</li>
									<li class="d-flex justify-content-between p-2 bg-light">
										<div class="text text-black font-weight-bold">Grand total</div>
										<div class="value text-right text-black font-weight-bold">0.00 THB</div>
									</li>
									<li class="d-flex justify-content-between align-items-center p-1 pl-2 pr-2">
										<div class="text text-black font-weight-normal d-flex flex-nowrap align-items-center justify-content-end">
											
										<span class="mr-1">Please pay within              </span>
											<select class="form-control select2-normal" data-width="50">
												<option selected>7</option>
												<option>5</option>
												<option>3</option>
												<option>1</option>
											</select>
											<span class="ml-1 mr-1">Days. Thank you for your business.</span>
										</div>
										
									</li>
									<li class="d-flex justify-content-between p-2">
										<div class="text text-black font-weight-normal">Remark : <a href="#" class="text-decoration-none"><img src="di/ic-remark.png" height="20"></a></div>
										
									</li>
								</ul>
							</div>
							
					</div>
					</div>
					
					<div class="ctrl-btn d-flex justify-content-end align-items-center">
						<select class="form-control select2-normal" data-width="70">
										<option value="df" selected>Draf</option>
										<option value="df">Publish</option>
						</select>
						<button type="button" class="btn btn-gray2 t-white ml-1 rounded-05">Preview</button>
						<button type="button" class="btn btn-gray2 t-white ml-1 rounded-05">Save</button>
						
					</div>

                </div>
            </div>
        </div>

    </main>

    

    <?php include("incs/js.html") ?>
	<?php include("incs/modal.html") ?>
	
    <script src="js/vendor/bootstrap-datepicker.js"></script>
    <script src="js/dore.script.js"></script>
    <script src="js/scripts.js"></script>
	<script>
	$(document).ready(function() {
	   $('.select2-normal').select2({
			//placeholder: 'Content Language',
			minimumResultsForSearch: -1,
			//width: 350
		});
	} );
	</script>
<script>
$(document).ready(function() {
	$('.main-menu>.scroll>.list-unstyled>li>a.rotate-arrow-icon').addClass('collapsed');
	$('.main-menu>.scroll>.list-unstyled>li>.collapse').removeClass('show');
	$('.main-menu .inner-level-menu>li').removeClass('active');
	$('.main-menu>.scroll>.list-unstyled>li:nth-child(3)>a.rotate-arrow-icon').removeClass('collapsed');
	$('.main-menu>.scroll>.list-unstyled>li:nth-child(3)>.collapse').addClass('show');
	$('.main-menu>.scroll>.list-unstyled>li:nth-child(3) .inner-level-menu>li:nth-child(4)').addClass('active');
});
</script>
</body>

</html>