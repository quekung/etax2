<!DOCTYPE html>
<html lang="en">

<!-- Top Head -->
<?php include("incs/header-top.html") ?>
<!-- /Top Head -->

<body id="app-container" class="menu-sub-hidden show-spinner">
    <?php include("incs/header.html") ?>
    <?php include("incs/sidebar-left.html") ?>

    <main>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="mb-2">
                        <h1 class="text-primary">Search Usage Report</h1>
						
						<?php /*?><nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
							<ol class="breadcrumb pt-0">
								<!--<li class="breadcrumb-item">
									<a href="#">Home</a>
								</li>-->
								<li class="breadcrumb-item">
									<a href="#">Import E-Stamp</a>
								</li>
								<li class="breadcrumb-item active text-gray" aria-current="page">Upload File</li>
								
							</ol>
						</nav><?php */?>

                    </div>
					
					
					
					<div class="card mb-3">
					<div class="card-body p-3">
						<div id="smart">

							


                            <div class="card-body">

                                    <h3 class="pb-2 h6">Output Vat Report</h3>
                                    <div class="row">
										<div class="col-12 col-sm-3 mb-3">
											<div class="input-group date">
													<input type="text" class="input-sm form-control border-right-0" name="start" placeholder="Start date *" />
													<span class="input-group-text input-group-append input-group-addon border-left-0">
														<i class="simple-icon-calendar"></i>
													</span>
												</div>

										</div>
										<div class="col-12 col-sm-3 mb-3">
											<div class="input-group date">
													<input type="text" class="input-sm form-control border-right-0" name="end" placeholder="End date *" />
													<span class="input-group-text input-group-append input-group-addon border-left-0">
														<i class="simple-icon-calendar"></i>
													</span>
												</div>
										</div>

			
										<div class="col-12 col-sm-6 mb-3">
											<label class="form-group has-float-label">
												<div class="input-group addon">
													<input type="text" class="input-sm form-control border-right-0" name="end" placeholder="Tag ID" />
													<span class="input-group-text input-group-append input-group-addon border-left-0">
														<i class="simple-icon-magnifier"></i>
													</span>
												</div>
												<span>Tag ID *</span>
											</label>
										</div>
									</div>
									
									<div id="compare">
										
										<div class="row">
											<div class="col">
												<h3 class="pb-2 h6">Branch</h3>
												<div class="border-light border rounded-05 p-2 h-100">
													<div class="custom-control custom-checkbox mb-1">
														<input type="checkbox" class="custom-control-input" id="branch1">
														<label class="custom-control-label" for="branch1">สาขา 1</label>
													</div>
													<div class="custom-control custom-checkbox mb-1">
														<input type="checkbox" class="custom-control-input" id="branch2">
														<label class="custom-control-label" for="branch2">สาขา 2</label>
													</div>
													<div class="custom-control custom-checkbox mb-1">
														<input type="checkbox" class="custom-control-input" id="branch3">
														<label class="custom-control-label" for="branch3">สาขา 3</label>
													</div>
												</div>
											</div>
											<div class="col-auto pt-5">
												<div class="mb-1"><button class="btn btn-xs btn-gray2 rounded-05 pt-0 pb-0" type="button"><i class="iconsminds-arrow-right"></i></button></div>
												<div class="mb-1"><button class="btn btn-xs btn-blue rounded-05 pt-0 pb-0" type="button"><i class="iconsminds-arrow-right-2"></i></button></div>
												<div class="mb-1"><button class="btn btn-xs btn-gray2 rounded-05 pt-0 pb-0" type="button"><i class="iconsminds-arrow-left"></i></button></div>
											</div>
											<div class="col">
												<h3 class="pb-2 h6">Choosed Branch</h3>
												<div class="border-light border rounded-05 p-2 h-100">
													
												</div>
											</div>
											
										</div>
									</div>
									
									<div class="ctrl-btn d-flex justify-content-end clearfix mt-5">
										<a href="setup-usage-report.php" class="btn btn-outline-primary btn-lg rounded-05 mr-2">Back</a>
										<a href="setup-usage-report-list.php" class="btn btn-primary btn-lg rounded-05">Search</a>
									</div>
									
                                
								
                            </div>
                        </div>

							
					</div>
					
	
					
					</div>
					
					<!-- end -->

                </div>
            </div>
        </div>

    </main>

    

    <?php include("incs/js.html") ?>
	<?php include("incs/modal.html") ?>


	
	<script src="js/vendor/jquery.smartWizard.min.js"></script>
    <script src="js/vendor/bootstrap-datepicker.js"></script>
    <script src="js/dore.script.js"></script>
    <script src="js/scripts.js"></script>
	<script>
	$(document).ready(function() {
	   $('.select2-normal').select2({
			//placeholder: 'Content Language',
			minimumResultsForSearch: -1,
			//width: 350
		});
	} );
	</script>
<script>
$(document).ready(function() {
	$('.main-menu>.scroll>.list-unstyled>li>a.rotate-arrow-icon').addClass('collapsed');
	$('.main-menu>.scroll>.list-unstyled>li>.collapse').removeClass('show');
	$('.main-menu .inner-level-menu>li').removeClass('active');
	$('.main-menu>.scroll>.list-unstyled>li:nth-child(8)>a.rotate-arrow-icon').removeClass('collapsed');
	$('.main-menu>.scroll>.list-unstyled>li:nth-child(8)>.collapse').addClass('show');
	$('.main-menu>.scroll>.list-unstyled>li:nth-child(8) .inner-level-menu>li:nth-child(2)').addClass('active');
});
</script>
</body>

</html>