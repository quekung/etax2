<!DOCTYPE html>
<html lang="en">

<!-- Top Head -->
<?php include("incs/header-top.html") ?>
<!-- /Top Head -->

<body id="app-container" class="menu-sub-hidden show-spinner">
    <?php include("incs/header.html") ?>
    <?php include("incs/sidebar-left.html") ?>

    <main>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="mb-2">
                        <h1 class="text-primary">iRich Co.ltd.,</h1>
						
						<?php /*?><nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
							<ol class="breadcrumb pt-0">
								<!--<li class="breadcrumb-item">
									<a href="#">Home</a>
								</li>-->
								<li class="breadcrumb-item">
									<a href="#">Import E-Stamp</a>
								</li>
								<li class="breadcrumb-item active text-gray" aria-current="page">Upload File</li>
								
							</ol>
						</nav><?php */?>
						
						<div class="top-right-button-container">
							<a href="setup-company-create.php" style="min-width: 90px;" class="btn btn-warning btn-sm rounded-05" readonly>Edit</a>
						</div>

                    </div>
					
					
					
					<div class="card mb-3">
					<div class="card-body p-3">
						<div id="smart" class="sw-main sw-theme-dots">
                            <ul class="card-header nav nav-tabs step-anchor step-tabs" role="tablist">
                                <li class="nav-item active">
									<a id="first-tab" href="setup-company-view.php" class="nav-link text">
									<i>1</i>
									<span>General Profile</span>
									</a>
								</li>
                                <li class="nav-item">
									<a id="second-tab" href="setup-company-view2.php" class="nav-link text">
									<i>2</i>
									<span>Branch Profile</span>
									</a>
								</li>
								 <li class="nav-item">
									<a id="second-tab" href="setup-company-view3.php" class="nav-link text">
									<i>3</i>
									<span>Email Server</span>
									</a>
								</li>
								 <li class="nav-item">
									<a id="second-tab" href="setup-company-view4.php" class="nav-link text">
									<i>4</i>
									<span>Config CA</span>
									</a>
								</li>
                            </ul>
							


                            <div class="card-body">
                                <div id="dotStep1" class="tab-pane show active" role="tabpanel" aria-labelledby="first-tab">
      
                                   	<div class="row setup-avatar align-items-end">
										<figure class="col-auto">
											<div class="border rounded-05 border-dark"><img class="d-block  rounded-05 bg-white text-gray" readonly src="di/logo-square.png" height="200"></div>
										</figure>
										<div class="col">
											<!--<div class="mb-5">
												<span class="alert-warning pl-3 pr-3 pt-2 pb-2 border-left">
													<b>Note : </b>Change profile image then update to profile your page
												</span>
											</div>-->
											<label class="form-group mb-3 has-float-label">
													<input type="text" class="input-sm form-control rounded-05 bg-white text-gray" readonly value="iRich Co.ltd.,">
												<span>Company name *</span>
											</label>
										</div>
									</div>
									
									<div class="row">
										<div class="col-6 mb-3">
											<label class="form-group mb-0 has-float-label">
													<input type="text" class="input-sm form-control rounded-05 bg-white text-gray" readonly value="9999999999">
												<span>Tag ID *</span>
											</label>
										</div>
										<div class="col-6 mb-3">
											<label class="form-group mb-0 input-group addon">
													<input type="text" class="input-sm form-control border-right-0 bg-white text-gray" readonly value="Parent Company">
													<span class="input-group-text input-group-append input-group-addon border-left-0">
														<i class="simple-icon-magnifier"></i></span>
											</label>

										</div>
										
										<div class="col-6 mb-3">
											<label class="form-group mb-0 has-float-label">
													<input type="text" class="input-sm form-control rounded-05 bg-white text-gray" readonly value="1683">
												<span>Buiding Number *</span>
											</label>
										</div>
										<div class="col-6 mb-3">
											<label class="form-group mb-0 input-group addon">
													<input type="text" class="input-sm form-control border-right-0 bg-white text-gray" readonly value="Building name">
													<span class="input-group-text input-group-append input-group-addon border-left-0">
														<i class="simple-icon-magnifier"></i></span>
											</label>
										</div>
										
										<div class="col-12 mb-3">
											<label class="form-group mb-0 has-float-label">
												<textarea class="form-control rounded-05 bg-white text-gray" readonly rows="2" name="jQueryDetail" required="">ถนนพหลโยธิน เซ็นทรัลพลาซ่าลาดพร้าว 1</textarea>
												<span>Address *</span>
											</label>
										</div>
										
										<div class="col-6 mb-3">
											<label class="form-group mb-0 has-float-label">
													<input type="text" class="input-sm form-control rounded-05 bg-white text-gray" readonly value="จตุจักร">
												<span>Sub District *</span>
											</label>
										</div>
										<div class="col-6 mb-3">
											<label class="form-group mb-0 has-float-label">
													<input type="text" class="input-sm form-control rounded-05 bg-white text-gray" readonly value="จตุจักร">
												<span>District *</span>
											</label>
										</div>
										
										<div class="col-6 mb-3">
											<label class="form-group mb-0 has-float-label">
													<input type="text" class="input-sm form-control rounded-05 bg-white text-gray" readonly value="กรุงเทพมหานคร">
												<span>Province *</span>
											</label>
										</div>
										<div class="col-6 mb-3">
											<label class="form-group mb-0 has-float-label">
													<input type="text" class="input-sm form-control rounded-05 bg-white text-gray" readonly value="10900">
												<span>Pose code *</span>
											</label>
										</div>
										
										<div class="col-6 mb-3">
											<label class="form-group mb-0 has-float-label">
													<input type="text" class="input-sm form-control rounded-05 bg-white text-gray" readonly value="etaxpoc@gmail.com">
												<span>Email *</span>
											</label>
										</div>
										<div class="col-6 mb-3">
											<label class="form-group mb-0 has-float-label">
													<input type="text" class="input-sm form-control rounded-05 bg-white text-gray" readonly value="+6629999999">
												<span>Telephone number *</span>
											</label>
										</div>
									</div>
									
									

									
									<div class="ctrl-btn d-flex justify-content-between mt-1">
										<div class="d-flex align-items-center">
											<label class="mb-0 mr-2 text-dark">Status</label>
											<div class="custom-control custom-checkbox d-inline-block disabled">

												<input type="checkbox" class="custom-control-input" id="jQueryCustomCheck1" name="jQueryCheckbox" disabled checked>
												<label class="custom-control-label text-gray" for="jQueryCustomCheck1">Active</label>
											</div>
										</div>
										
										<div class="top-right-button-container">
										<!--<a href="setup-role.php" class="btn btn-outline-primary btn-lg rounded-05 mr-2">Previous</a>-->
										<a href="setup-company-view2.php" class="btn btn-primary btn-lg rounded-05">Next</a>
										</div>
									</div>
									
                                </div>
								
                            </div>
                        </div>

							
					</div>
					
	
					
					</div>
					
					<!-- end -->

                </div>
            </div>
        </div>

    </main>

    

    <?php include("incs/js.html") ?>
	<?php include("incs/modal.html") ?>


	
	<script src="js/vendor/jquery.smartWizard.min.js"></script>
    <script src="js/vendor/bootstrap-datepicker.js"></script>
    <script src="js/dore.script.js"></script>
    <script src="js/scripts.js"></script>
	<script>
	$(document).ready(function() {
	   $('.select2-normal').select2({
			//placeholder: 'Content Language',
			minimumResultsForSearch: -1,
			//width: 350
		});
	} );
	</script>
<script>
$(document).ready(function() {
	$('.main-menu>.scroll>.list-unstyled>li>a.rotate-arrow-icon').addClass('collapsed');
	$('.main-menu>.scroll>.list-unstyled>li>.collapse').removeClass('show');
	$('.main-menu .inner-level-menu>li').removeClass('active');
	$('.main-menu>.scroll>.list-unstyled>li:nth-child(8)>a.rotate-arrow-icon').removeClass('collapsed');
	$('.main-menu>.scroll>.list-unstyled>li:nth-child(8)>.collapse').addClass('show');
	$('.main-menu>.scroll>.list-unstyled>li:nth-child(8) .inner-level-menu>li:nth-child(1)').addClass('active');
});
</script>
</body>

</html>