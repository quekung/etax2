<!DOCTYPE html>
<html lang="en">

<!-- Top Head -->
<?php include("incs/header-top.html") ?>
<!-- /Top Head -->

<body id="app-container" class="menu-sub-hidden show-spinner">
    <?php include("incs/header.html") ?>
    <?php include("incs/sidebar-left.html") ?>

    <main>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="mb-2">
                        <h1 class="text-blue">Document Volume</h1>
						
                    </div>

					
					<div class="row">
						<div class="col-12 mb-4">
							<div class="card">
								<div class="card-body">
						
									
									<div class="float-left float-none-xs">
										<div class="d-inline-block">
											<h5 class="d-inline">Consent Graph</h5>
											<!--<span class="text-muted text-small d-block">Unique Visitors</span>-->
										</div>
									</div>
									<div class="btn-group float-right float-none-xs mt-2">
										<button class="btn btn-outline-primary btn-xs dropdown-toggle" type="button"
											data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
											This Week
										</button>
										<div class="dropdown-menu">
											<a class="dropdown-item" href="#">Last Week</a>
											<a class="dropdown-item" href="#">This Month</a>
										</div>
									</div>
									
								</div>
								<div class="card-body">
									<div class="dashboard-line-chart chart">
										<canvas id="salesChart"></canvas>
									</div>
								</div>
							</div>
						</div>
					</div>
					
					<div class="row">
						<div class="col-12">
							<!-- list dashboard -->
							<div class="box-ds mb-4">
								<div class="title-bar d-flex justify-content-between align-items-end mb-3">
									<h3 class="h5 mb-0 text-blue">Current month</h3>
									<!--<a href="reconcile-list.php" class="btn btn-primary top-right-button rounded-05 text-small"><i class="icon-img"><img src="di/ic-view-wh.png" height="16"></i> VIEW SOURCE</a>-->
								</div>
								<ul class="row list-inline chd-card">
									 <li class="col-12 col-sm-6 col-md-3">
									   <div class="card">
											<div class="card-body p-3">

														<p class="mb-0 label font-weight-bold">Generate XML</p>
														<p class="font-weight-bold text-success mb-2 value">&nbsp;</p>
														
														<div class="row d-flex flex-nowrap justify-content-between align-items-end">
															<span class="col-4 sp-left">&nbsp;</span>
															<p class="lead mb-0 text-primary text-center font-weight-bold text-large"><big>1</big></p>
															<span class="col-4 p-0 text-muted">Transaction</span>
														</div>
														
														
										   </div>
										   <div class="card-footer text-center">
										   		<span class="text-danger">Remain 15</span>
										   </div>
									   </div>
									 </li>
									 
									<li class="col-12 col-sm-6 col-md-3">
									   <div class="card">
											<div class="card-body p-3">

														<p class="mb-0 label font-weight-bold">Generate PDF</p>
														<p class="font-weight-bold text-success mb-2 value">&nbsp;</p>
														
														<div class="row d-flex flex-nowrap justify-content-between align-items-end">
															<span class="col-4 sp-left">&nbsp;</span>
															<p class="lead mb-0 text-success text-center font-weight-bold text-large"><big>0</big></p>
															<span class="col-4 p-0 text-muted">Transaction</span>
														</div>
														
														
										   </div>
										   <div class="card-footer text-center">
										   		<span class="text-danger">Remain 15</span>
										   </div>
									   </div>
									 </li>
									 
									 <li class="col-12 col-sm-6 col-md-3">
									   <div class="card">
											<div class="card-body p-3">

														<p class="mb-0 label font-weight-bold">Send E-Mail PDF</p>
														<p class="font-weight-bold text-success mb-2 value">&nbsp;</p>
														
														<div class="row d-flex flex-nowrap justify-content-between align-items-end">
															<span class="col-4 sp-left">&nbsp;</span>
															<p class="lead mb-0 text-orange text-center font-weight-bold text-large"><big>0</big></p>
															<span class="col-4 p-0 text-muted">Transaction</span>
														</div>
														
														
										   </div>
										   <div class="card-footer text-center">
										   		<span class="text-danger">Remain 15</span>
										   </div>
									   </div>
									 </li>
									 
									 <li class="col-12 col-sm-6 col-md-3">
									   <div class="card">
											<div class="card-body p-3">

														<p class="mb-0 label font-weight-bold">Send RD XML</p>
														<p class="font-weight-bold text-success mb-2 value">&nbsp;</p>
														
														<div class="row d-flex flex-nowrap justify-content-between align-items-end">
															<span class="col-4 sp-left">&nbsp;</span>
															<p class="lead mb-0 text-dark text-center font-weight-bold text-large"><big>0</big></p>
															<span class="col-4 p-0 text-muted">Transaction</span>
														</div>
														
														
										   </div>
										   <div class="card-footer text-center">
										   		<span class="text-danger">Remain 15</span>
										   </div>
									   </div>
									 </li>
									 
								</ul>
							</div>
							<!-- /list dashboard -->
							
							<!-- list dashboard -->
							<div class="box-ds mb-4">
								<div class="title-bar d-flex justify-content-between align-items-end mb-3">
									<h3 class="h5 mb-0 text-blue">Document Volume</h3>
									<!--<a href="reconcile-list.php" class="btn btn-primary top-right-button rounded-05 text-small"><i class="icon-img"><img src="di/ic-view-wh.png" height="16"></i> VIEW SOURCE</a>-->
								</div>
								<ul class="row list-inline chd-card">
									 <li class="col-12 col-sm-6 col-md-3">
									   <div class="card">
											<div class="card-body p-3">

														<p class="mb-0 label font-weight-bold">Generate XML</p>
														<p class="font-weight-bold text-success mb-2 value">&nbsp;</p>
														
														<div class="row d-flex flex-nowrap justify-content-between align-items-end">
															<span class="col-4 sp-left">&nbsp;</span>
															<p class="lead mb-0 text-primary text-center font-weight-bold text-large"><big>147</big></p>
															<span class="col-4 p-0 text-muted">Transaction</span>
														</div>
														
														
										   </div>
										   <div class="card-footer text-center">
										   		<span class="text-danger">Remain 15</span>
										   </div>
									   </div>
									 </li>
									 
									<li class="col-12 col-sm-6 col-md-3">
									   <div class="card">
											<div class="card-body p-3">

														<p class="mb-0 label font-weight-bold">Generate PDF</p>
														<p class="font-weight-bold text-success mb-2 value">&nbsp;</p>
														
														<div class="row d-flex flex-nowrap justify-content-between align-items-end">
															<span class="col-4 sp-left">&nbsp;</span>
															<p class="lead mb-0 text-success text-center font-weight-bold text-large"><big>147</big></p>
															<span class="col-4 p-0 text-muted">Transaction</span>
														</div>
														
														
										   </div>
										   <div class="card-footer text-center">
										   		<span class="text-danger">Remain 15</span>
										   </div>
									   </div>
									 </li>
									 
									 <li class="col-12 col-sm-6 col-md-3">
									   <div class="card">
											<div class="card-body p-3">

														<p class="mb-0 label font-weight-bold">Send E-Mail PDF</p>
														<p class="font-weight-bold text-success mb-2 value">&nbsp;</p>
														
														<div class="row d-flex flex-nowrap justify-content-between align-items-end">
															<span class="col-4 sp-left">&nbsp;</span>
															<p class="lead mb-0 text-orange text-center font-weight-bold text-large"><big>5</big></p>
															<span class="col-4 p-0 text-muted">Transaction</span>
														</div>
														
														
										   </div>
										   <div class="card-footer text-center">
										   		<span class="text-danger">Remain 15</span>
										   </div>
									   </div>
									 </li>
									 
									 <li class="col-12 col-sm-6 col-md-3">
									   <div class="card">
											<div class="card-body p-3">

														<p class="mb-0 label font-weight-bold">Send RD XML</p>
														<p class="font-weight-bold text-success mb-2 value">&nbsp;</p>
														
														<div class="row d-flex flex-nowrap justify-content-between align-items-end">
															<span class="col-4 sp-left">&nbsp;</span>
															<p class="lead mb-0 text-dark text-center font-weight-bold text-large"><big>100</big></p>
															<span class="col-4 p-0 text-muted">Transaction</span>
														</div>
														
														
										   </div>
										   <div class="card-footer text-center">
										   		<span class="text-danger">Remain 15</span>
										   </div>
									   </div>
									 </li>
									 
								</ul>
							</div>
							<!-- /list dashboard -->
						</div>
						
						<div class="col-12 mb-4">
							<div class="card ds-table">
								<div class="card-body">
									<h3 class="h5 card-title text-primary">Previous Month Operations</h3>

									<table class="table table-striped table-responsive-xs">
										<thead>
											<tr>
												<th scope="col">Action</th>
												<th scope="col" class="text-center">All Document</th>
												<th scope="col" class="text-center">Process</th>
												<th scope="col" class="text-center">Remains</th>
												<th scope="col" class="text-center">Complete (%)</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td class="text-left"><span class="badge badge-outline-success mb-1">Generate PDF</span></td>
												<td class="text-center">161</td>
												<td class="text-center"><span class="text-dark">34</span></td>
												<td class="text-center"><span class="text-dark">14</span></td>
												<td class="text-center"><span class="text-orange">91.3 %</span></td>
											</tr>
											
											<tr>
												<td class="text-left"><span class="badge badge-outline-danger mb-1">Send E-Mail PDF</span></td>
												<td class="text-center">161</td>
												<td class="text-center"><span class="text-dark">34</span></td>
												<td class="text-center"><span class="text-dark">14</span></td>
												<td class="text-center"><span class="text-orange">91.3 %</span></td>
											</tr>
											
											<tr>
												<td class="text-left"><span class="badge badge-outline-warning mb-1">Send RD XML</span></td>
												<td class="text-center">161</td>
												<td class="text-center"><span class="text-dark">34</span></td>
												<td class="text-center"><span class="text-dark">14</span></td>
												<td class="text-center"><span class="text-orange">91.3 %</span></td>
											</tr>
											
											<tr>
												<td class="text-left"><span class="badge badge-outline-dark mb-1">Generate XML</span></td>
												<td class="text-center">161</td>
												<td class="text-center"><span class="text-dark">34</span></td>
												<td class="text-center"><span class="text-dark">14</span></td>
												<td class="text-center"><span class="text-orange">91.3 %</span></td>
											</tr>
											
										</tbody>
									</table>
								</div>
							</div>
						
						</div>
						
					</div>
					
                </div>
            </div>
        </div>

    </main>

    

    <?php include("incs/js.html") ?>
	
	 <script src="js/vendor/Chart.bundle.min.js"></script>
    <script src="js/vendor/chartjs-plugin-datalabels.js"></script>
	 
    <script src="js/dore.script.js"></script>
    <script src="js/scripts.js"></script>
	<script>
/*	$(document).ready(function() {
    $('#tablelist').DataTable( {
        "paging":   false,
        "ordering": false,
        "info":     false
    } );
} );*/
	</script>
<script>
$(document).ready(function() {
	$('.main-menu>.scroll>.list-unstyled>li>a.rotate-arrow-icon').addClass('collapsed');
	$('.main-menu>.scroll>.list-unstyled>li>.collapse').removeClass('show');
	$('.main-menu .inner-level-menu>li').removeClass('active');
	//$('.main-menu>.scroll>.list-unstyled>li:nth-child(1)>a.rotate-arrow-icon').removeClass('collapsed');
	//$('.main-menu>.scroll>.list-unstyled>li:nth-child(1)>.collapse').addClass('show');
	$('.main-menu>.scroll>.list-unstyled>li:nth-child(1)').addClass('active');
});
</script>
</body>

</html>