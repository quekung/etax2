<!DOCTYPE html>
<html lang="en">

<!-- Top Head -->
<?php include("incs/header-top.html") ?>
<!-- /Top Head -->

<body id="app-container" class="menu-sub-hidden show-spinner">
    <?php include("incs/header.html") ?>
    <?php include("incs/sidebar-left.html") ?>

    <main>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="mb-2">
                        <h1 class="text-primary">Generate XML</h1>
						
						<?php /*?><nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
							<ol class="breadcrumb pt-0">
								<!--<li class="breadcrumb-item">
									<a href="#">Home</a>
								</li>-->
								<li class="breadcrumb-item">
									<a href="#">Import E-Stamp</a>
								</li>
								<li class="breadcrumb-item active text-gray" aria-current="page">Upload File</li>
								
							</ol>
						</nav><?php */?>

                    </div>
					
					
					
					<div class="card mb-3">
					<div class="card-body p-3">
							<div class="search-bar">
							<a class="btn p-2 d-inline-block d-md-none" data-toggle="collapse" href="#searchOptions" role="button" aria-expanded="true" aria-controls="searchOptions">
								Display Options
								<i class="simple-icon-arrow-down align-middle"></i>
							</a>
							<div id="searchOptions">
								<div class="d-flex flex-wrap row align-items-end">
									<div class="col-12 col-sm mb-2">

									   <select class="custom-select border-top-0 border-left-0 border-right-0" required="">
											<option value="" selected>iRich Co.ltd., สำนักงานใหญ่ (00000)</option>
											<option value="1">One</option>
											<option value="2">Two</option>
											<option value="3">Three</option>
										</select>
									</div>
									<div class="col-12 col-sm input-group flex-nowrap mb-2">

									   <label class="form-group has-float-label">
											<input class="form-control datepicker border-top-0 border-left-0 border-right-0" placeholder="25/08/2020">
											<span>Start Date</span>
										</label>
										<div class="input-group-append">
											<span class="input-group-text border-top-0 border-left-0 border-right-0"><i class="simple-icon-calendar"></i></span>
										</div>
									</div>
									
									<div class="col-12 col-sm input-group flex-nowrap mb-2">

									   <label class="form-group has-float-label">
											<input class="form-control datepicker border-top-0 border-left-0 border-right-0" placeholder="31/08/2020">
											<span>End Date</span>
										</label>
										<div class="input-group-append">
											<span class="input-group-text border-top-0 border-left-0 border-right-0"><i class="simple-icon-calendar"></i></span>
										</div>
									</div>
									
									

									<div class="top-right-button-container text-nowrap col-12 col-sm-auto mb-2">

												<button class="btn btn-primary btn-md top-right-button rounded-05" type="button" id="btnSearch" style="min-width: 120px"> <i class="icon-img"><img src="di/ic-search-wh.png" height="20"></i> Search</button>
									</div>
								</div>
							</div>
							</div>
							
							
							<ul class="nav nav-tabs separator-tabs mt-5 mb-5 position-relative" role="tablist">
								<li class="nav-item">
									<a class="nav-link active" id="first-tab" data-toggle="tab" href="#first" role="tab" aria-controls="first" aria-selected="true">By Day</a>
								</li>

								<li class="nav-item">
									<a class="nav-link" id="second-tab" data-toggle="tab" href="#second" role="tab" aria-controls="second" aria-selected="false">By Month</a>
								</li>
								
								<li class="text-nowrap position-absolute fix-top-right">
									<a href="javascript:;" href="javascript:;" data-toggle="modal" data-target="#removeModalconfirm" class="btn btn-blue top-right-button rounded-05"><i class="icon-img f-white mt-1n"><img src="di/ic-gen.png" height="14"></i> Generate</a>
								</li>
							</ul>
							
							<!--<div class="headbar-tb mt-4 mb-0 d-flex justify-content-end align-items-center">
								
							</div>-->
							
							<div class="tab-content">
                        		<!-- tab1 -->
								<div class="tab-pane show active" id="first" role="tabpanel" aria-labelledby="first-tab">
								
									<div class="dc-add-tb dc-min-h ds-table mt-4">
										<table class="table table-striped table-responsive-xs">
												<thead>
													<tr>

														<th scope="col" class="text-left">
															<div class="btn-group">

																	<label class="custom-control custom-checkbox mb-0 mr-n2 d-inline-block">
																		<input type="checkbox" class="custom-control-input" id="checkAll">
																		<span class="custom-control-label">&nbsp;</span>
																	</label>
																</div>
																<button type="button" class="btn btn-xs btn-transparent dropdown-toggle dropdown-toggle-split pl-0 pr-0" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
																	<span class="sr-only">Toggle Dropdown</span>
																</button>
																<div class="dropdown-menu dropdown-menu-right">
																	<a class="dropdown-item" href="#">Check All</a>
																	<a class="dropdown-item" href="#">None</a>
																</div>
															</div>
														</th>
														<th scope="col">Year</th>
														<th scope="col">Date</th>
														<th scope="col">Document Type</th>
														<th scope="col">Document Count</th>
														<th scope="col">Branch Code</th>
													</tr>
												</thead>
												<tbody>
													<?php for($i=1;$i<=1;$i++){ ?>
													<tr>
														<td valign="middle" class="text-left">
															<label class="custom-control custom-checkbox mb-0 align-self-center mr-4 mb-1">
																<input type="checkbox" class="custom-control-input">
																<span class="custom-control-label">&nbsp;</span>
															</label>
														</td>
														<td valign="middle" class="text-left">2020</td>
														<td valign="middle" class="text-left">25/08/2020</td>
														<td valign="middle" class="text-left">1</td>
														<td valign="middle" class="text-left">Invoice/Tax Invoice</td>
														<td valign="middle" class="text-left">0000<?php echo $i; ?></td>
													</tr>
													<?php } ?>



												</tbody>
											</table>
									</div>

									<div class="ft-paging d-flex justify-content-between align-items-center">
										<div class="dropdown-as-select display-page" id="pageCount">
											<span class="text-black text-small">1-10 of 195 items</span>
										</div>
										<div class="d-block d-md-inline-block ml-5">
											<nav class="ctrl-page d-flex flex-nowrap align-items-center">
												<ul class="pagination justify-content-center mb-0">
												   <!-- <li class="page-item ">
														<a class="page-link first" href="#">
															<i class="simple-icon-control-start"></i>
														</a>
													</li>-->
													<li class="page-item ">
														<a class="page-link prev" href="#">
															<i class="simple-icon-arrow-left"></i>
														</a>
													</li>
													<li class="page-item active">
														<a class="page-link" href="#">1</a>
													</li>
													<li class="page-item ">
														<a class="page-link" href="#">2</a>
													</li>
													<li class="page-item">
														<a class="page-link" href="#">3</a>
													</li>
													<li class="page-item">
														<a class="page-link" href="#">4</a>
													</li>
													<li class="page-item">
														<a class="page-link" href="#">5</a>
													</li>
													<li class="page-item">
														<span class="page-link">...</span>
													</li>
													<li class="page-item">
														<a class="page-link" href="#">19</a>
													</li>
													<li class="page-item ">
														<a class="page-link next" href="#" aria-label="Next">
															<i class="simple-icon-arrow-right"></i>
														</a>
													</li>
													<!--<li class="page-item ">
														<a class="page-link last" href="#">
															<i class="simple-icon-control-end"></i>
														</a>
													</li>-->
												</ul>

													<button class="btn btn-outline-dark btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
														12
													</button>
													<div class="dropdown-menu dropdown-menu-right">
														<a class="dropdown-item" href="#">5</a>
														<a class="dropdown-item active" href="#">12</a>
														<a class="dropdown-item" href="#">24</a>
													</div>
											</nav>
										</div>


									</div>
								
								</div>
								<!-- tab2 -->
								<div class="tab-pane fade" id="second" role="tabpanel" aria-labelledby="second-tab">
								Month
								</div>
							</div>
							

							
							

							
					</div>
					</div>
					
					<!-- end -->

                </div>
            </div>
        </div>

    </main>

    

    <?php include("incs/js.html") ?>
	<?php include("incs/modal.html") ?>
	
    <script src="js/vendor/bootstrap-datepicker.js"></script>
    <script src="js/dore.script.js"></script>
    <script src="js/scripts.js"></script>
	<script>
	$(document).ready(function() {
	   $('.select2-normal').select2({
			//placeholder: 'Content Language',
			minimumResultsForSearch: -1,
			//width: 350
		});
	} );
	</script>
<script>
$(document).ready(function() {
	$('.main-menu>.scroll>.list-unstyled>li>a.rotate-arrow-icon').addClass('collapsed');
	$('.main-menu>.scroll>.list-unstyled>li>.collapse').removeClass('show');
	$('.main-menu .inner-level-menu>li').removeClass('active');
	$('.main-menu>.scroll>.list-unstyled>li:nth-child(3)>a.rotate-arrow-icon').removeClass('collapsed');
	$('.main-menu>.scroll>.list-unstyled>li:nth-child(3)>.collapse').addClass('show');
	$('.main-menu>.scroll>.list-unstyled>li:nth-child(3) .inner-level-menu>li:nth-child(15)').addClass('active');
});
</script>
</body>

</html>