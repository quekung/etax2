<!DOCTYPE html>
<html lang="en">

<!-- Top Head -->
<?php include("incs/header-top.html") ?>
<!-- /Top Head -->

<body id="app-container" class="menu-sub-hidden show-spinner">
    <?php include("incs/header.html") ?>
    <?php include("incs/sidebar-left.html") ?>

    <main>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="mb-2">
                        <h1 class="text-primary">Import File</h1>
						
						<?php /*?><nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
							<ol class="breadcrumb pt-0">
								<!--<li class="breadcrumb-item">
									<a href="#">Home</a>
								</li>-->
								<li class="breadcrumb-item">
									<a href="#">Import E-Stamp</a>
								</li>
								<li class="breadcrumb-item active text-gray" aria-current="page">Upload File</li>
								
							</ol>
						</nav><?php */?>

                    </div>
					
					<div class="card mb-3">
					<div class="card-body p-3">

						<div class="row-dl-upload d-flex flex-wrap justify-content-between align-items-center">
                                <p class="mb-1 col-12 col-sm"><!--<img class="mt-n3 mr-2" src="di/ic-download-fromupload.png" height="40"></i>--> <span class="d-inline-block mt-2 text-muted">ดาวน์โหลดไฟล์ตัวอย่าง เทมเพลท  อัพโหลด</span></p>
                                <a href="import-estamp-upload-list.php" class="btn btn-blue rounded-05 btn-multiple-state" id="successButton">
                                    <div class="spinner d-inline-block">
                                        <div class="bounce1"></div>
                                        <div class="bounce2"></div>
                                        <div class="bounce3"></div>
                                    </div>
                                    <span class="icon success" data-toggle="tooltip" data-placement="top" title="" data-original-title="Everything went right!">
                                        <i class="simple-icon-check"></i>
                                    </span>
                                    <span class="icon fail" data-toggle="tooltip" data-placement="top" title="" data-original-title="Something went wrong!">
                                        <i class="simple-icon-exclamation"></i>
                                    </span>
                                    <span class="label"><i class="icon-img f-white mt-1n"><img src="di/ic-download-fromupload.png" height="16"></i> CSV</span>
                                </a>
                            </div>

                    
						
						
						
					</div>
					</div>
					
					<div class="card mb-3">
					<div class="card-body p-3">
							<div class="wrap-drop mb-2 border-dash">
								<form action="/file-upload">
									<div class="dropzone">
									</div>
								</form>
							</div>
							<p class="mb-0 d-flex flex-wrap justify-content-between">
								<a href="import-estamp-upload-list.php" class="text-primary">tmb_2020_loan_agreement.csv</a>
								<span><i class="icon-img"><img src="di/ic-check.png" height="14"></i><!--<i class="glyph-icon simple-icon-close"></i>--></span>
								<small class="d-black col-12 p-0 text-light">278 KB</small>
							</p>
							
					</div>
					</div>

                </div>
            </div>
        </div>

    </main>

    

    <?php include("incs/js.html") ?>
	
	 <script src="js/vendor/Chart.bundle.min.js"></script>
    <script src="js/vendor/chartjs-plugin-datalabels.js"></script>
	<script src="js/vendor/dropzone.min.js"></script>
    <script src="js/dore.script.js"></script>
    <script src="js/scripts.js"></script>
	<script>
/*	$(document).ready(function() {
    $('#tablelist').DataTable( {
        "paging":   false,
        "ordering": false,
        "info":     false
    } );
} );*/
	</script>
<script>
$(document).ready(function() {
	$('.main-menu>.scroll>.list-unstyled>li>a.rotate-arrow-icon').addClass('collapsed');
	$('.main-menu>.scroll>.list-unstyled>li>.collapse').removeClass('show');
	$('.main-menu .inner-level-menu>li').removeClass('active');
	$('.main-menu>.scroll>.list-unstyled>li:nth-child(2)>a.rotate-arrow-icon').removeClass('collapsed');
	$('.main-menu>.scroll>.list-unstyled>li:nth-child(2)>.collapse').addClass('show');
	$('.main-menu>.scroll>.list-unstyled>li:nth-child(2) .inner-level-menu>li:nth-child(1)').addClass('active');
});
</script>
</body>

</html>