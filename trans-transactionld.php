<!DOCTYPE html>
<html lang="en">

<!-- Top Head -->
<?php include("incs/header-top.html") ?>
<!-- /Top Head -->

<body id="app-container" class="menu-sub-hidden show-spinner">
    <?php include("incs/header.html") ?>
    <?php include("incs/sidebar-left.html") ?>

    <main>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="mb-2">
                        <h1 class="text-primary">Transactionld : 193</h1>
						
						<?php /*?><nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
							<ol class="breadcrumb pt-0">
								<!--<li class="breadcrumb-item">
									<a href="#">Home</a>
								</li>-->
								<li class="breadcrumb-item">
									<a href="#">Import E-Stamp</a>
								</li>
								<li class="breadcrumb-item active text-gray" aria-current="page">Upload File</li>
								
							</ol>
						</nav><?php */?>
						
						<div class="top-right-button-container">

                            <div class="btn-group">
                               <div class="btn btn-tranparent ml-5">Resent Count : 0</div>
							   <div class="btn btn-tranparent ml-5">Creat Date : 02/09/2020</div>
							   <div class="btn btn-tranparent ml-5">Sent Date : -</div>
                            </div>
                        </div>

                    </div>
					
					
					
					<div class="card mb-3">
					<div class="card-body p-3">
						<div id="transID">
           

                            <div class="card-body">
                                <div id="dotStep2" class="tab-pane show active" role="tabpanel" aria-labelledby="first-tab">
									
									<div class="dc-add-tb dc-min-h ds-table mt-0">
										<table class="table table-striped table-responsive-xs">
												<thead>
													<tr>
														<th scope="col">Document No</th>
														<th scope="col">Document Type</th>
														<th scope="col">Document Date</th>
														<th scope="col">Amount</th>
													</tr>
												</thead>
												<tbody>
													<?php for($i=1;$i<=10;$i++){ ?>
													<tr>

														<td>TV67677654<?php echo $i; ?></td>
														<td>
															ใบแจ้งหนี้/ใบกำกับภาษี
														</td>
														<td>
															25/08/2020
														</td>
														<td>
															17,500.00
														</td>
													</tr>
													<?php } ?>
												</tbody>
										</table>
									</div>
									<?php /*?>
									
									<div class="ft-paging d-flex justify-content-between align-items-center">
										<div class="dropdown-as-select display-page" id="pageCount">
											<span class="text-black text-small">1-10 of 195 items</span>
										</div>
										<div class="d-block d-md-inline-block ml-5">
											<nav class="ctrl-page d-flex flex-nowrap align-items-center">
												<ul class="pagination justify-content-center mb-0">
												   <!-- <li class="page-item ">
														<a class="page-link first" href="#">
															<i class="simple-icon-control-start"></i>
														</a>
													</li>-->
													<li class="page-item ">
														<a class="page-link prev" href="#">
															<i class="simple-icon-arrow-left"></i>
														</a>
													</li>
													<li class="page-item active">
														<a class="page-link" href="#">1</a>
													</li>
													<li class="page-item ">
														<a class="page-link" href="#">2</a>
													</li>
													<li class="page-item">
														<a class="page-link" href="#">3</a>
													</li>
													<li class="page-item">
														<a class="page-link" href="#">4</a>
													</li>
													<li class="page-item">
														<a class="page-link" href="#">5</a>
													</li>
													<li class="page-item">
														<span class="page-link">...</span>
													</li>
													<li class="page-item">
														<a class="page-link" href="#">19</a>
													</li>
													<li class="page-item ">
														<a class="page-link next" href="#" aria-label="Next">
															<i class="simple-icon-arrow-right"></i>
														</a>
													</li>
													<!--<li class="page-item ">
														<a class="page-link last" href="#">
															<i class="simple-icon-control-end"></i>
														</a>
													</li>-->
												</ul>

													<button class="btn btn-outline-dark btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
														12
													</button>
													<div class="dropdown-menu dropdown-menu-right">
														<a class="dropdown-item" href="#">5</a>
														<a class="dropdown-item active" href="#">12</a>
														<a class="dropdown-item" href="#">24</a>
													</div>
											</nav>
										</div>


									</div>
<?php */?>


									
									
                                </div>
								
                            </div>
                        </div>

							
					</div>
					
	
					
					</div>
					
					<!-- end -->

                </div>
            </div>
        </div>

    </main>

    

    <?php include("incs/js.html") ?>
	<?php include("incs/modal.html") ?>
<!-- Modal -->
<div class="modal fade" id="viewResultModal" tabindex="-1" role="dialog"	aria-hidden="true">
	<div class="modal-dialog" role="document">
		
		<div class="modal-content">
			<div class="modal-header  pt-3 pb-2 border-0">
				<h5 class="modal-title p-2">Search Result</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body pt-0 pb-5">
			   <div class="dc-add-tb  ds-table mt-0">
					<table class="table table-striped table-responsive-xs">
							<thead>
								<tr>

									<th scope="col">
										<label class="custom-control custom-checkbox mb-0 mr-n3 d-inline-block">
											<input type="checkbox" class="custom-control-input">
											<span class="custom-control-label">&nbsp;</span>
										</label>
									</th>
									<th scope="col">Document Type</th>
									<th scope="col">Files Amount</th>
								</tr>
							</thead>
							<tbody>
								<?php for($i=1;$i<=1;$i++){ ?>
								<tr>
									<td>
										<label class="custom-control custom-checkbox mb-0 mr-n3 d-inline-block">
											<input type="checkbox" class="custom-control-input">
											<span class="custom-control-label">&nbsp;</span>
										</label>
									</td>
									<td>
										Tax Invoice
									</td>
									<td>
										8
									</td>
								</tr>
								<?php } ?>
							</tbody>
					</table>
				</div>
			   
			   
			</div>
			<div class="modal-footer pt-2 d-flex justify-content-end border-0">
				<button type="button" class="btn btn-md btn-outline-primary rounded-05 col-3">View</button>	 <button type="button" class="btn btn-md btn-primary rounded-05 col-3">Confirm</button>			
			</div>

		</div>
	</div>
</div>
<!-- /Modal -->

	
	<script src="js/vendor/jquery.smartWizard.min.js"></script>
    <script src="js/vendor/bootstrap-datepicker.js"></script>
    <script src="js/dore.script.js"></script>
    <script src="js/scripts.js"></script>
	<script>
	$(document).ready(function() {
	   $('.select2-normal').select2({
			//placeholder: 'Content Language',
			minimumResultsForSearch: -1,
			//width: 350
		});
	} );
	</script>
<script>
$(document).ready(function() {
	$('.main-menu>.scroll>.list-unstyled>li>a.rotate-arrow-icon').addClass('collapsed');
	$('.main-menu>.scroll>.list-unstyled>li>.collapse').removeClass('show');
	$('.main-menu .inner-level-menu>li').removeClass('active');
	$('.main-menu>.scroll>.list-unstyled>li:nth-child(4)>a.rotate-arrow-icon').removeClass('collapsed');
	$('.main-menu>.scroll>.list-unstyled>li:nth-child(4)>.collapse').addClass('show');
	$('.main-menu>.scroll>.list-unstyled>li:nth-child(4) .inner-level-menu>li:nth-child(1)').addClass('active');
});
</script>
</body>

</html>