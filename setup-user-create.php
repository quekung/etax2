<!DOCTYPE html>
<html lang="en">

<!-- Top Head -->
<?php include("incs/header-top.html") ?>
<!-- /Top Head -->

<body id="app-container" class="menu-sub-hidden show-spinner">
    <?php include("incs/header.html") ?>
    <?php include("incs/sidebar-left.html") ?>

    <main>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="mb-2">
                        <h1 class="text-primary">Edit User</h1>
						
						<?php /*?><nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
							<ol class="breadcrumb pt-0">
								<!--<li class="breadcrumb-item">
									<a href="#">Home</a>
								</li>-->
								<li class="breadcrumb-item">
									<a href="#">Import E-Stamp</a>
								</li>
								<li class="breadcrumb-item active text-gray" aria-current="page">Upload File</li>
								
							</ol>
						</nav><?php */?>
						
						<div class="top-right-button-container">
							<a href="setup-role.php" style="min-width: 90px;" class="btn btn-outline-primary btn-sm rounded-05 mr-2">Cancle</a>
							<a href="setup-role.php" style="min-width: 90px;" class="btn btn-primary btn-sm rounded-05">Save</a>
						</div>

                    </div>
					
					
					
					<div class="card mb-3">
					<div class="card-body p-3">
						<div id="smart" class="sw-main sw-theme-dots">
                            <ul class="card-header nav nav-tabs step-anchor step-tabs" role="tablist">
                                <li class="nav-item active">
									<a id="first-tab" href="setup-user-create.php" class="nav-link text">
									<i>1</i>
									<span>User Profile</span>
									</a>
								</li>
                                <li class="nav-item">
									<a id="second-tab" href="setup-user-create2.php" class="nav-link text">
									<i>2</i>
									<span>Set Company</span>
									</a>
								</li>
								 <li class="nav-item">
									<a id="second-tab" href="setup-user-create3.php" class="nav-link text">
									<i>3</i>
									<span>Set Branch</span>
									</a>
								</li>
								 <li class="nav-item">
									<a id="second-tab" href="setup-user-create4.php" class="nav-link text">
									<i>4</i>
									<span>Set Role</span>
									</a>
								</li>
                            </ul>
							


                            <div class="card-body">
                                <div id="dotStep1" class="tab-pane show active" role="tabpanel" aria-labelledby="first-tab">
      
                                   
									<div class="row">
										<div class="col-6 mb-3">
											<div class="form-group mb-0">
												<label class="hid">Username *</label>
												<input type="text" class="input-sm form-control rounded-05" placeholder="Username*" />
												</div>
										</div>
										<div class="col-6 mb-3">
											<div class="form-group mb-0">
												<label class="hid">First Name Last Name</label>
												<input type="text" class="input-sm form-control rounded-05" placeholder="First Name Last Name" />
												</div>
										</div>
										
										<div class="col-6 mb-3">
											<div class="form-group mb-0">
												<label class="hid">Password *</label>
												<input type="password" class="input-sm form-control rounded-05" placeholder="Password*" />
												</div>
										</div>
										<div class="col-6 mb-3">
											<div class="form-group mb-0">
												<label class="hid">Comfirm Password</label>
												<input type="password" class="input-sm form-control rounded-05" placeholder="Comfirm Password *" />
												</div>
										</div>
									</div>
									
									<div class="row">
										<div class="col-6 mb-3">
											<label class="form-group has-float-label">
												<div class="input-group addon">
													<span class="input-group-text input-group-append input-group-addon border-right-0">
														<select class="custom-select border-0 pt-0 pb-0 pl-0 pr-4" required="" style="height: 20px">
															<option selected>+66</option>
															<option>+67</option>
														</select>
														<!--<select class="form-control select2-normal" data-width="60" data-placeholder="code">
															<option></option>
															<option selected>+66</option>
															<option>+67</option>
														</select>-->
													</span>
													<div class="form-control border-left-0">
														<input type="text" class="input-sm border-0" name="end" placeholder="Tag ID">
													</div>
													
												</div>
												<span>Telephone *</span>
											</label>
											
										</div>
									</div>


									
									<div class="ctrl-btn d-flex justify-content-end clearfix mt-5">
										<!--<a href="setup-role.php" class="btn btn-outline-primary btn-lg rounded-05 mr-2">Previous</a>-->
										<a href="setup-user-create2.php" class="btn btn-primary btn-lg rounded-05">Next</a>
									</div>
									
                                </div>
								
                            </div>
                        </div>

							
					</div>
					
	
					
					</div>
					
					<!-- end -->

                </div>
            </div>
        </div>

    </main>

    

    <?php include("incs/js.html") ?>
	<?php include("incs/modal.html") ?>


	
	<script src="js/vendor/jquery.smartWizard.min.js"></script>
    <script src="js/vendor/bootstrap-datepicker.js"></script>
    <script src="js/dore.script.js"></script>
    <script src="js/scripts.js"></script>
	<script>
	$(document).ready(function() {
	   $('.select2-normal').select2({
			//placeholder: 'Content Language',
			minimumResultsForSearch: -1,
			//width: 350
		});
	} );
	</script>
<script>
$(document).ready(function() {
	$('.main-menu>.scroll>.list-unstyled>li>a.rotate-arrow-icon').addClass('collapsed');
	$('.main-menu>.scroll>.list-unstyled>li>.collapse').removeClass('show');
	$('.main-menu .inner-level-menu>li').removeClass('active');
	$('.main-menu>.scroll>.list-unstyled>li:nth-child(9)>a.rotate-arrow-icon').removeClass('collapsed');
	$('.main-menu>.scroll>.list-unstyled>li:nth-child(9)>.collapse').addClass('show');
	$('.main-menu>.scroll>.list-unstyled>li:nth-child(9) .inner-level-menu>li:nth-child(1)').addClass('active');
});
</script>
</body>

</html>