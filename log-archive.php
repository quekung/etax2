<!DOCTYPE html>
<html lang="en">

<!-- Top Head -->
<?php include("incs/header-top.html") ?>
<!-- /Top Head -->

<body id="app-container" class="menu-sub-hidden show-spinner">
    <?php include("incs/header.html") ?>
    <?php include("incs/sidebar-left.html") ?>

    <main>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="mb-2">
                        <h1 class="text-primary">Archive and Purge</h1>
						
						<?php /*?><nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
							<ol class="breadcrumb pt-0">
								<!--<li class="breadcrumb-item">
									<a href="#">Home</a>
								</li>-->
								<li class="breadcrumb-item">
									<a href="#">Import E-Stamp</a>
								</li>
								<li class="breadcrumb-item active text-gray" aria-current="page">Upload File</li>
								
							</ol>
						</nav><?php */?>

                    </div>
					
					
					
					<div class="card mb-3">
					<div class="card-body p-3">

										
							<ul class="nav nav-tabs separator-tabs mt-0 mb-5 position-relative" role="tablist">
								<li class="nav-item">
									<a class="nav-link active" id="first-tab" data-toggle="tab" href="#first" role="tab" aria-controls="first" aria-selected="true">Archive</a>
								</li>

								<li class="nav-item">
									<a class="nav-link " id="second-tab" data-toggle="tab" href="#second" role="tab" aria-controls="second" aria-selected="false">Purge</a>
								</li>

							</ul>
							

							
							<div class="tab-content">
                        		<!-- tab1 -->
								<div class="tab-pane show active" id="first" role="tabpanel" aria-labelledby="first-tab">
							

									<div class="row">
										<div class="col-12">
											<label class="form-group has-float-label">
												<input class="form-control" placeholder="File Name *" />
												<span>File Name <span class="text-danger">*</span></span>
											</label>
										</div>
									</div>
									
									<div class="row">
										<div class="col-6">
											 <label class="form-group has-float-label fix-cal-right">
												<input class="form-control datepicker" placeholder="Start date">
												<span class="input-group-text position-absolute float-right border-0"><i class="simple-icon-calendar"></i></span>
												<span>Start date <span class="text-danger">*</span></span>
											</label>
											
										</div>
										<div class="col-6">
											<label class="form-group has-float-label fix-cal-right">
												<input class="form-control datepicker" placeholder="End date">
												<span class="input-group-text position-absolute float-right border-0"><i class="simple-icon-calendar"></i></span>
												<span>End date <span class="text-danger">*</span></span>
											</label>
										</div>
									</div>
									
									<div class="d-flex justify-content-end mt-0">
										<button class="btn btn-primary btn-md top-right-button rounded-05" type="button" id="btnProcess" onclick="$('#default').remove(); $('.tb-list').removeClass('invisible');">Process</button>
									</div>

								</div>
								<!-- tab2 -->
								<div class="tab-pane fade" id="second" role="tabpanel" aria-labelledby="second-tab">
									
									<div class="row">
										<div class="col-6">
											 <label class="form-group has-float-label fix-cal-right">
												<input class="form-control datepicker" placeholder="Start date">
												<span class="input-group-text position-absolute float-right border-0"><i class="simple-icon-calendar"></i></span>
												<span>Start date <span class="text-danger">*</span></span>
											</label>
											
										</div>
										<div class="col-6">
											<label class="form-group has-float-label fix-cal-right">
												<input class="form-control datepicker" placeholder="End date">
												<span class="input-group-text position-absolute float-right border-0"><i class="simple-icon-calendar"></i></span>
												<span>End date <span class="text-danger">*</span></span>
											</label>
										</div>
									</div>
									
									<div class="d-flex justify-content-end mt-0">
										<button class="btn btn-primary btn-md top-right-button rounded-05" type="button" id="btnProcess" onclick="$('#default').remove(); $('.tb-list').removeClass('invisible');">Process</button>
									</div>
									
								</div>
							</div>

							
					</div>
					</div>
					
					<!-- end -->

                </div>
            </div>
        </div>

    </main>

    

    <?php include("incs/js.html") ?>
	<?php include("incs/modal.html") ?>
	
    <script src="js/vendor/bootstrap-datepicker.js"></script>
    <script src="js/dore.script.js"></script>
    <script src="js/scripts.js"></script>
	<script>
	$(document).ready(function() {
	   $('.select2-normal').select2({
			//placeholder: 'Content Language',
			minimumResultsForSearch: -1,
			//width: 350
		});
	} );
	</script>
<script>
$(document).ready(function() {
	$('.main-menu>.scroll>.list-unstyled>li>a.rotate-arrow-icon').addClass('collapsed');
	$('.main-menu>.scroll>.list-unstyled>li>.collapse').removeClass('show');
	$('.main-menu .inner-level-menu>li').removeClass('active');
	$('.main-menu>.scroll>.list-unstyled>li:nth-child(6)>a.rotate-arrow-icon').removeClass('collapsed');
	$('.main-menu>.scroll>.list-unstyled>li:nth-child(6)>.collapse').addClass('show');
	$('.main-menu>.scroll>.list-unstyled>li:nth-child(6) .inner-level-menu>li:nth-child(2)').addClass('active');
});
</script>
</body>

</html>