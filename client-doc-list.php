<!DOCTYPE html>
<html lang="en">

<!-- Top Head -->
<?php include("incs/header-top.html") ?>
<!-- /Top Head -->

<body id="app-container">
    	<header id="header-micro" class="d-flex justify-content-center align-items-center p-2 bg-white">
			<h1 class="micro-logo mb-0 pb-0"><a class="mr-2 ml-2" href="#"><img src="di/logo-client.png" height="60"></a> <span class="h6 text-black font-weight-bold">บริษัท ดีจีแอล จำกัด</span></h1>
		</header>


        <div class="main-client container-fluid pt-5">
            <div class="row">
				<!-- col-l -->
				<div class="col-12 col-lg-4">
                    <div class="mb-2">
                        <h1 class="h5 text-primary">ข้อมูลสมาชิก</h1>
                    </div>
					
					<div class="card h-100 mb-3">
					<div class="card-body p-3">
						<ul class="client-info">
							<li><em>รหัสผู้ใช้งาน : </em><div>640807903</div></li>
							<li><em>ชื่อ-สกุล : </em><div>สมคิด จิรานันตรัตน์</div></li>
							<li><em>เบอร์โทรศัพท์ : </em><div>081 6678879</div></li>
							<li><em>ที่อยู่ : </em><div>267/1 ซอย สาธุประดิษฐ์ 15 แขวง ช่องนนทรี เขต ยานนาวา กรุงเทพมหานคร 10120</div></li>
							<li><em>บริการที่รับสิทธิ์แล้ว : </em><div>3BB Member, 3BB Cloud TV, 3BB Cloudbox</div></li>
						</ul>
					</div>
					</div>
					
				</div>
				<!-- col-r -->
                <div class="col-12  col-lg-8">
                    <div class="mb-2">
                        <h1 class="h5 text-primary">บิลอิเล็กทรอนิกส์</h1>
                    </div>

					<div class="card h-100 mb-3">
					<div class="card-body p-3">

			
							

							
							<div class="dc-add-tb ds-table">
								<table class="table table-striped table-responsive-xs">
										<thead>
											<tr>
												
												<th scope="col">เลขที่ใบแจ้งหนี้</th>
												<th scope="col">รายการ</th>
												<th scope="col" class="text-center">กำหนดชำระ</th>
												<th scope="col" class="text-center">ยอดชำระ/ปรับปรุง</th>
												<th scope="col" class="text-center">ดาวน์โหลดสำเนา</th>
				
											</tr>
										</thead>
										<tbody>
											<?php for($i=1;$i<=6;$i++){ ?>
											<tr>

												<td valign="middle" class="text-left"><a class="text-black" href="#">4631116100664<?php echo $i; ?></a></td>
												<td valign="middle" class="text-left">ค่าบริการอินเตอร์เน็ต เดือน ธันวาคม 2563</td>
												<td valign="middle" class="text-left">25/12/2020</td>
												<td valign="middle" class="text-left">500.00</td>
												<td valign="middle" class="text-center">
													<a href="#" title="PDF"><img src="di/ic-save-pdf.png" height="30"></a>
												</td>

												
												
											</tr>
											<?php } ?>
											
											
											
										</tbody>
									</table>
							</div>
							

							
							<div class="ft-paging d-flex justify-content-between align-items-center">
								<div class="dropdown-as-select display-page" id="pageCount">
									<span class="text-black text-small">1-10 of 195 items</span>
								</div>
								<div class="d-block d-md-inline-block ml-5">
									<nav class="ctrl-page d-flex flex-nowrap align-items-center">
										<ul class="pagination justify-content-center mb-0">
										   <!-- <li class="page-item ">
												<a class="page-link first" href="#">
													<i class="simple-icon-control-start"></i>
												</a>
											</li>-->
											<li class="page-item ">
												<a class="page-link prev" href="#">
													<i class="simple-icon-arrow-left"></i>
												</a>
											</li>
											<li class="page-item active">
												<a class="page-link" href="#">1</a>
											</li>
											<li class="page-item ">
												<a class="page-link" href="#">2</a>
											</li>
											<li class="page-item">
												<a class="page-link" href="#">3</a>
											</li>
											<li class="page-item">
												<a class="page-link" href="#">4</a>
											</li>
											<li class="page-item">
												<a class="page-link" href="#">5</a>
											</li>
											<li class="page-item">
												<span class="page-link">...</span>
											</li>
											<li class="page-item">
												<a class="page-link" href="#">19</a>
											</li>
											<li class="page-item ">
												<a class="page-link next" href="#" aria-label="Next">
													<i class="simple-icon-arrow-right"></i>
												</a>
											</li>
											<!--<li class="page-item ">
												<a class="page-link last" href="#">
													<i class="simple-icon-control-end"></i>
												</a>
											</li>-->
										</ul>
										
											<button class="btn btn-outline-dark btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
												12
											</button>
											<div class="dropdown-menu dropdown-menu-right">
												<a class="dropdown-item" href="#">5</a>
												<a class="dropdown-item active" href="#">12</a>
												<a class="dropdown-item" href="#">24</a>
											</div>
									</nav>
								</div>
								
								
							</div>

							
					</div>
					</div>
					
					<!-- end -->

            </div>
        </div>

    

    <?php include("incs/js.html") ?>
	<?php include("incs/modal.html") ?>
	
    <script src="js/vendor/bootstrap-datepicker.js"></script>
    <script src="js/dore.script.js"></script>
    <script src="js/scripts.js"></script>
	<script>
	$(document).ready(function() {
	   $('.select2-normal').select2({
			//placeholder: 'Content Language',
			minimumResultsForSearch: -1,
			//width: 350
		});
	} );
	</script>
<script>
$(document).ready(function() {
	$('.main-menu>.scroll>.list-unstyled>li>a.rotate-arrow-icon').addClass('collapsed');
	$('.main-menu>.scroll>.list-unstyled>li>.collapse').removeClass('show');
	$('.main-menu .inner-level-menu>li').removeClass('active');
	$('.main-menu>.scroll>.list-unstyled>li:nth-child(3)>a.rotate-arrow-icon').removeClass('collapsed');
	$('.main-menu>.scroll>.list-unstyled>li:nth-child(3)>.collapse').addClass('show');
	$('.main-menu>.scroll>.list-unstyled>li:nth-child(3) .inner-level-menu>li:nth-child(17)').addClass('active');
});
</script>
</body>

</html>