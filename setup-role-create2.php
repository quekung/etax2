<!DOCTYPE html>
<html lang="en">

<!-- Top Head -->
<?php include("incs/header-top.html") ?>
<!-- /Top Head -->

<body id="app-container" class="menu-sub-hidden show-spinner">
    <?php include("incs/header.html") ?>
    <?php include("incs/sidebar-left.html") ?>

    <main>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="mb-2">
                        <h1 class="text-primary">Creat Role</h1>
						
						<?php /*?><nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
							<ol class="breadcrumb pt-0">
								<!--<li class="breadcrumb-item">
									<a href="#">Home</a>
								</li>-->
								<li class="breadcrumb-item">
									<a href="#">Import E-Stamp</a>
								</li>
								<li class="breadcrumb-item active text-gray" aria-current="page">Upload File</li>
								
							</ol>
						</nav><?php */?>
						
						<div class="top-right-button-container">
							<a href="setup-role.php" style="min-width: 90px;" class="btn btn-outline-primary btn-sm rounded-05 mr-2">Cancle</a>
							<a href="setup-role.php" style="min-width: 90px;" class="btn btn-primary btn-sm rounded-05">Save</a>
						</div>

                    </div>
					
					
					
					<div class="card mb-3">
					<div class="card-body p-3">
						<div id="smart" class="sw-main sw-theme-dots">
                            <ul class="card-header nav nav-tabs step-anchor step-tabs" role="tablist">
                                <li class="nav-item ">
									<a id="first-tab" href="setup-role-create.php" class="nav-link text">
									<i>1</i>
									<span>Set Name</span>
									</a>
								</li>
                                <li class="nav-item active">
									<a id="second-tab" href="setup-role-create2.php" class="nav-link text">
									<i>2</i>
									<span>Set Aurthorization</span>
									</a>
								</li>
                            </ul>
							


                            <div class="card-body">
                                <div id="dotStep2" class="tab-pane show active" role="tabpanel" aria-labelledby="first-tab">
      
                                   
									<div class="dc-add-tb dc-min-h ds-table mt-4">
										<table class="table table-striped table-responsive-xs">
												<thead>
													<tr>
														<th scope="col">Document</th>
														<th scope="col" width="40%">
															<label class="custom-control custom-checkbox mb-0 mr-n3 d-inline-block">
																<input type="checkbox" class="custom-control-input">
																<span class="custom-control-label">&nbsp;</span>
															</label>
														</th>
													</tr>
												</thead>
												<tbody>
													<?php for($i=1;$i<=12;$i++){ ?>
													<tr class="tb-list">
														
														<td valign="middle" class="text-left">

															<?php if($i==1){ ?>
																Debit Note
															<?php } elseif($i==2){ ?>
															 Invoice
															<?php } elseif($i==3){ ?>
															Generate XML
															<?php } elseif($i==4){ ?>
															Download
															<?php } elseif($i==5){ ?>
															 Invoice / Tax Invoice
															 <?php } elseif($i==6){ ?>
															  Receipt / Tax Invoice
															 <?php } elseif($i==7){ ?>
															 Credit Note
															  <?php } elseif($i==8){ ?>
															 Cancellation Note
															  <?php } elseif($i==9){ ?>
															 Abbreviated Invoice
															  <?php } elseif($i==10){ ?>
															Receipt
															  <?php } elseif($i==11){ ?>
															Quotation
															<?php } else { ?>
																Document List (Sell)
															<?php } ?>
														</td>
														
														<td valign="middle" class="text-left">
															<label class="custom-control custom-checkbox mb-0 mr-n3 d-inline-block">
																<input type="checkbox" class="custom-control-input" checked>
																<span class="custom-control-label">&nbsp;</span>
															</label>
														</td>



													</tr>
													<?php } ?>

												</tbody>
											</table>
											
											
											<table class="table table-striped table-responsive-xs">
												<thead>
													<tr>
														<th scope="col">Transaction management</th>
														<th scope="col" width="40%">
															<label class="custom-control custom-checkbox mb-0 mr-n3 d-inline-block">
																<input type="checkbox" class="custom-control-input">
																<span class="custom-control-label">&nbsp;</span>
															</label>
														</th>
													</tr>
												</thead>
												<tbody>
													<?php for($i=1;$i<=5;$i++){ ?>
													<tr class="tb-list">
														
														<td valign="middle" class="text-left">

															<?php if($i==1){ ?>
																 Send file to RD
															<?php } elseif($i==2){ ?>
															Job Schedule
															<?php } elseif($i==3){ ?>
															Process Monitor
															<?php } elseif($i==4){ ?>
															Import Reconcile
															<?php } else { ?>
															Reconcile Process
															<?php } ?>
														</td>
														
														<td valign="middle" class="text-left">
															<label class="custom-control custom-checkbox mb-0 mr-n3 d-inline-block">
																<input type="checkbox" class="custom-control-input" checked>
																<span class="custom-control-label">&nbsp;</span>
															</label>
														</td>



													</tr>
													<?php } ?>

												</tbody>
											</table>
									</div>

									
									<div class="ctrl-btn d-flex justify-content-end clearfix mt-5">
										<a href="setup-role.php" class="btn btn-outline-primary btn-lg rounded-05 mr-2">Previous</a>
										<a href="setup-role-create2.php" class="btn btn-primary btn-lg rounded-05">Confirm</a>
									</div>
									
                                </div>
								
                            </div>
                        </div>

							
					</div>
					
	
					
					</div>
					
					<!-- end -->

                </div>
            </div>
        </div>

    </main>

    

    <?php include("incs/js.html") ?>
	<?php include("incs/modal.html") ?>


	
	<script src="js/vendor/jquery.smartWizard.min.js"></script>
    <script src="js/vendor/bootstrap-datepicker.js"></script>
    <script src="js/dore.script.js"></script>
    <script src="js/scripts.js"></script>
	<script>
	$(document).ready(function() {
	   $('.select2-normal').select2({
			//placeholder: 'Content Language',
			minimumResultsForSearch: -1,
			//width: 350
		});
	} );
	</script>
<script>
$(document).ready(function() {
	$('.main-menu>.scroll>.list-unstyled>li>a.rotate-arrow-icon').addClass('collapsed');
	$('.main-menu>.scroll>.list-unstyled>li>.collapse').removeClass('show');
	$('.main-menu .inner-level-menu>li').removeClass('active');
	$('.main-menu>.scroll>.list-unstyled>li:nth-child(9)>a.rotate-arrow-icon').removeClass('collapsed');
	$('.main-menu>.scroll>.list-unstyled>li:nth-child(9)>.collapse').addClass('show');
	$('.main-menu>.scroll>.list-unstyled>li:nth-child(9) .inner-level-menu>li:nth-child(2)').addClass('active');
});
</script>
</body>

</html>