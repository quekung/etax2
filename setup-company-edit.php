<!DOCTYPE html>
<html lang="en">

<!-- Top Head -->
<?php include("incs/header-top.html") ?>
<!-- /Top Head -->

<body id="app-container" class="menu-sub-hidden show-spinner">
    <?php include("incs/header.html") ?>
    <?php include("incs/sidebar-left.html") ?>

    <main>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="mb-2">
                        <h1 class="text-primary">Running Doc No</h1>
						
						<?php /*?><nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
							<ol class="breadcrumb pt-0">
								<!--<li class="breadcrumb-item">
									<a href="#">Home</a>
								</li>-->
								<li class="breadcrumb-item">
									<a href="#">Import E-Stamp</a>
								</li>
								<li class="breadcrumb-item active text-gray" aria-current="page">Upload File</li>
								
							</ol>
						</nav><?php */?>

                    </div>
					
					
					
					<div class="card mb-3">
					<div class="card-body p-3">
							<div class="search-bar">
							<a class="btn p-2 d-inline-block d-md-none" data-toggle="collapse" href="#searchOptions" role="button" aria-expanded="true" aria-controls="searchOptions">
								Display Options
								<i class="simple-icon-arrow-down align-middle"></i>
							</a>
							<div id="searchOptions">
								<div class="d-flex flex-wrap row align-items-end">
									<div class="col-12 col-sm input-group flex-nowrap mb-2">
										<?php /*?><div class="input-group-append">
											<span class="input-group-text border-0 pb-0 d-flex align-item-end"><i class="simple-icon-magnifier"></i></span>
										</div>
										<label class="form-group has-float-label">
											<input class="form-control border-top-0 border-left-0 border-right-0" placeholder="Search...">
										</label><?php */?>
										
										
										<select class="custom-select border-top-0 border-left-0 border-right-0" required="">
											<option value="" selected="">iRich Co.ltd., สำนักงานใหญ่ (00000)</option>
											<option value="1">One</option>
											<option value="2">Two</option>
											<option value="3">Three</option>
										</select>
									</div>
									
									
									<div class="top-right-button-container text-nowrap col-12 col-sm-auto mb-2">

												<a class="btn btn-primary btn-md top-right-button rounded-05" href="javascript:;" data-toggle="modal" data-target="#saveCPNModalconfirm" style="min-width: 120px">Save</a>
									</div>
								</div>
							</div>
							</div>
										

							
							<div class="dc-add-tb dc-min-h ds-table mt-4">
								<table class="table table-striped table-responsive-xs">
										<thead>
											<tr>
												<th scope="col">No</th>
												<th scope="col">Document Name</th>
												<th scope="col">Channel</th>
												<th scope="col">Prefix</th>
												<th scope="col">Date Pattern</th>
												<th scope="col">Suffix</th>
												<th scope="col">Running Digit</th>
												<th scope="col">Current Running Number</th>
												<th scope="col">Seperate type</th>
											</tr>
										</thead>
										<tbody>
											<?php for($i=1;$i<=10;$i++){ ?>
											<tr class="tb-list">
												<td valign="middle" class="text-left"><?php echo $i; ?></td>
												<td valign="middle" class="text-left">
															<?php if($i==1){ ?>
															 Invoice / Tax Invoice
															 <?php } elseif($i==2){ ?>
															  Receipt / Tax Invoice
															 <?php } elseif($i==3){ ?>
															 Credit Note
															  <?php } elseif($i==4){ ?>
															 Cancellation Note
															  <?php } elseif($i==5){ ?>
															 Abbreviated Invoice
															  <?php } elseif($i==6){ ?>
															Receipt
															  <?php } elseif($i==7){ ?>
															Quotation
															<?php } else { ?>
																Document List (Sell)
															<?php } ?>
												</td>
												

												<td valign="middle" class="text-left">Auto</td>


												<td valign="middle" class="text-left">
													<input class="form-control form-control-sm" style="max-width: 120px" type="text" value="T02-">
												</td>
												<td valign="middle" class="text-left">
													<select class="custom-select border-top-0 border-left-0 border-right-0" required="">
														<option value="" selected="">yyyyMM</option>
														<option value="1">One</option>
														<option value="2">Two</option>
														<option value="3">Three</option>
													</select>
												</td>
												<td valign="middle" class="text-left">
													<input class="form-control form-control-sm" style="max-width: 120px" type="text">
												</td>
												<td valign="middle" class="text-left">
													<input class="form-control form-control-sm text-right" style="max-width: 120px" type="text" value="5">
												</td>
												<td valign="middle" class="text-left">
													<input class="form-control form-control-sm text-right" style="max-width: 120px" type="text" value="0">
												</td>
												<td valign="middle" class="text-left">
													<select class="custom-select border-top-0 border-left-0 border-right-0" required="">
														<option value="" selected="">Bymonth</option>
														<option value="1">One</option>
														<option value="2">Two</option>
														<option value="3">Three</option>
													</select>
												</td>
												
											</tr>
											<?php } ?>
											
											
											
										</tbody>
									</table>
							</div>
							

							
							<div class="ft-paging d-flex justify-content-between align-items-center">
								<div class="dropdown-as-select display-page" id="pageCount">
									<span class="text-black text-small">1-10 of 195 items</span>
								</div>
								<div class="d-block d-md-inline-block ml-5">
									<nav class="ctrl-page d-flex flex-nowrap align-items-center">
										<ul class="pagination justify-content-center mb-0">
										   <!-- <li class="page-item ">
												<a class="page-link first" href="#">
													<i class="simple-icon-control-start"></i>
												</a>
											</li>-->
											<li class="page-item ">
												<a class="page-link prev" href="#">
													<i class="simple-icon-arrow-left"></i>
												</a>
											</li>
											<li class="page-item active">
												<a class="page-link" href="#">1</a>
											</li>
											<li class="page-item ">
												<a class="page-link" href="#">2</a>
											</li>
											<li class="page-item">
												<a class="page-link" href="#">3</a>
											</li>
											<li class="page-item">
												<a class="page-link" href="#">4</a>
											</li>
											<li class="page-item">
												<a class="page-link" href="#">5</a>
											</li>
											<li class="page-item">
												<span class="page-link">...</span>
											</li>
											<li class="page-item">
												<a class="page-link" href="#">19</a>
											</li>
											<li class="page-item ">
												<a class="page-link next" href="#" aria-label="Next">
													<i class="simple-icon-arrow-right"></i>
												</a>
											</li>
											<!--<li class="page-item ">
												<a class="page-link last" href="#">
													<i class="simple-icon-control-end"></i>
												</a>
											</li>-->
										</ul>
										
											<button class="btn btn-outline-dark btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
												12
											</button>
											<div class="dropdown-menu dropdown-menu-right">
												<a class="dropdown-item" href="#">5</a>
												<a class="dropdown-item active" href="#">12</a>
												<a class="dropdown-item" href="#">24</a>
											</div>
									</nav>
								</div>
								
								
							</div>

							
					</div>
					</div>
					
					<!-- end -->

                </div>
            </div>
        </div>

    </main>

    <!-- Modal -->
<div class="modal fade" id="saveCPNModalconfirm" tabindex="-1" role="dialog"	aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header pb-0 border-0">

				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body p-0 text-center">
			   <i class="icon-img"><img src="di/ic-warning-orange.png" height="100"></i>
			   <h5 class="text-dark mt-3">Save running document number</h5>
				<p>Would you like to save document running ?</p>
			</div>
			<div class="modal-footer pt-2 d-flex justify-content-center border-0">
				<button type="button" class="btn btn-md btn-success rounded-05 col-3" data-dismiss="modal" data-toggle="modal" data-target="#SaveCPNModalsuccess">Confirm</button>
				<button type="button" class="btn btn-md btn-red rounded-05 col-3" data-dismiss="modal">Cancel</button>				
			</div>
		</div>
	</div>
</div>


<div class="modal fade" id="SaveCPNModalsuccess" tabindex="-1" role="dialog"	aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header pb-0 border-0">

				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body pt-0 pb-0 text-center">
			   <i class="icon-img"><img src="di/ic-check-success.png" height="100"></i>
			   <h5 class="text-dark mt-3">Transaction result</h5>
			   <p>Susccess save running doc no XX items</p>
			</div>
			<div class="modal-footer pt-2 d-flex justify-content-center border-0">
				<button type="button" class="btn btn-md btn-success rounded-05 col-3">OK</button>			
			</div>

		</div>
	</div>
</div>
<!-- /Modal -->    

    <?php include("incs/js.html") ?>
	<?php include("incs/modal.html") ?>
	
	
    <script src="js/vendor/bootstrap-datepicker.js"></script>
    <script src="js/dore.script.js"></script>
    <script src="js/scripts.js"></script>
	<script>
	$(document).ready(function() {
	   $('.select2-normal').select2({
			//placeholder: 'Content Language',
			minimumResultsForSearch: -1,
			//width: 350
		});
	} );
	</script>
<script>
$(document).ready(function() {
	$('.main-menu>.scroll>.list-unstyled>li>a.rotate-arrow-icon').addClass('collapsed');
	$('.main-menu>.scroll>.list-unstyled>li>.collapse').removeClass('show');
	$('.main-menu .inner-level-menu>li').removeClass('active');
	$('.main-menu>.scroll>.list-unstyled>li:nth-child(8)>a.rotate-arrow-icon').removeClass('collapsed');
	$('.main-menu>.scroll>.list-unstyled>li:nth-child(8)>.collapse').addClass('show');
	$('.main-menu>.scroll>.list-unstyled>li:nth-child(8) .inner-level-menu>li:nth-child(2)').addClass('active');
});
</script>
</body>

</html>